package net.biomodels.parameters;

import java.util.Objects;

/**
 * @author carankalle on 26/09/2018.
 */
@SuppressWarnings("WeakerAccess")
public class SearchView {
    private String entity;//SpeciesAnnotation
    private String entityId;//SpeciesIdOrName
    private String entityAccessionUrlString;
    private String reaction;
    private String reactionOriginal;
    private String model;
    private String externalLinks;
    private String publication;
    private String parameters;
    private String organism;
    private String rate;
    private String rateOriginal;
    private String assignment;
    private String assignmentOriginal;

    private String reactants;
    private String products;
    private String modifiers;
    private String initialData;
    private String reactionSBOTerm;
    private String reactionSBOTermUrlString;
    private String entitySBOTerm;
    private String entitySBOTermUrlString;
    private boolean isCurated;

    public String getReactionOriginal() {
        return reactionOriginal;
    }

    public String getReactionSBOTermUrlString() {
        return reactionSBOTermUrlString;
    }

    public String getEntitySBOTermUrlString() {
        return entitySBOTermUrlString;
    }

    public String getEntity() {
        return entity;
    }


    public String getOrganism() {
        return organism;
    }
    public String getEntityAccessionUrlString() {
        return entityAccessionUrlString;
    }

    public String getEntityId() {
        return entityId;
    }

    public String getReaction() {
        return reaction;
    }

    public String getModel() {
        return model;
    }

    public String getExternalLinks() {
        return externalLinks;
    }

    public String getPublication() {
        return publication;
    }

    public String getParameters() {
        return parameters;
    }

    public String getRate() {
        return rate;
    }

    public String getRateOriginal() {
        return rateOriginal;
    }

    public String getAssignment() {
        return assignment;
    }

    public String getAssignmentOriginal() {
        return assignmentOriginal;
    }

    public String getReactants() {
        return reactants;
    }

    public String getProducts() {
        return products;
    }

    public String getReactionSBOTerm() {
        return reactionSBOTerm;
    }

    public String getEntitySBOTerm() {
        return entitySBOTerm;
    }


    public String getInitialData() {
        return initialData;
    }

    public boolean getIsCurated() {
        return isCurated;
    }

    public static SearchView convertParamsModelToSearchView(ParamsModel paramsModel) {
        if (paramsModel.getShow()) {
            SearchView searchView = new SearchView();
            searchView.entity = paramsModel.getSpeciesAnnotation().isEmpty() ?
                (paramsModel.getSpeciesName().isEmpty() ? paramsModel.getSpeciesId() : paramsModel.getSpeciesName()) :
                paramsModel.getSpeciesAnnotation();
            searchView.entityId = paramsModel.getSpeciesId();
            searchView.entityAccessionUrlString = paramsModel.getAccessionUrlString();
            searchView.reaction = paramsModel.getReaction();
            searchView.reactionOriginal = paramsModel.getReactionOriginal();
            searchView.model = paramsModel.getModelID();
            searchView.externalLinks = paramsModel.getExternalLinks();
            searchView.publication = paramsModel.getReferencePublication();
            searchView.organism = paramsModel.getOrganism();
            searchView.parameters = paramsModel.getParameters();
            ReactionInfo reactionInfo = paramsModel.getReactionInfo();
            // FIXME, reactionInfo needs to exist for both rules and reactions
            if (paramsModel.getRateRule().isEmpty()) {
                searchView.rate = Objects.requireNonNull(reactionInfo).getRate();
                searchView.rateOriginal = paramsModel.getEquationOriginal();
            } else {
                searchView.rate = paramsModel.getRateRule();
                searchView.rateOriginal = paramsModel.getRateRuleOriginal();

            }
            searchView.assignment = paramsModel.getAssignmentRule();
            searchView.assignmentOriginal = paramsModel.getAssignmentRuleOriginal();

            searchView.initialData = paramsModel.getInitialData();

            searchView.reactants = reactionInfo.getReactantsField();
            searchView.products  = reactionInfo.getProductsField();
            searchView.modifiers = paramsModel.getModifiers();

            searchView.reactionSBOTerm = paramsModel.getReactionSBOTerm();
            searchView.entitySBOTerm = paramsModel.getSboTerm();
            searchView.reactionSBOTermUrlString = paramsModel.getReactionSBOTermUrlString();
            searchView.entitySBOTermUrlString = paramsModel.getSpeciesSboTermUrlString();
            searchView.isCurated = paramsModel.getIsCurated();
            return searchView;
        } else {
            return null;
        }
    }


    public String getModifiers() {
        return modifiers;
    }
}

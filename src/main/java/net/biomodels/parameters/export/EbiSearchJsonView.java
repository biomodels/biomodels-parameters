package net.biomodels.parameters.export;

import com.opencsv.CSVReaderHeaderAware;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author carankalle on 01/10/2018.
 */
public class EbiSearchJsonView {

    private String name;
    private String release;
    private String releaseDate;
    private long entryCount;
    private List<EbiSearchEntry> entries = new ArrayList<>();
    private static final List<String> rawFieldNames = Collections.unmodifiableList(
        Arrays.asList("entity",
            "parameters",
            "products",
            "reactants",
            "modifiers",
            "rate",
            "reaction",
            "reaction_original",
            "initial_data",
            "rate_original",
            "assignment_original"));

    private static final List<String> keepUnderScoreFields = Collections.unmodifiableList(
        Arrays.asList("publication"));

    public EbiSearchJsonView() {

        release = "1.0";
        entryCount = 0;
        name = "BioModels Parameters";
        releaseDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

    }


    private static String formatJsonKey(String key) throws IOException {
        key = key.replace(" ", "_").toLowerCase(new Locale("en"));
        return key;
    }

    private static String formatJsonValue(String value) throws IOException {
        value = value.replaceAll("\\+|\\-|\\||\\!|\\(|\\)|\\{|\\}|\\[|\\]|\\^|\\*|\\/", "\\\\$0");
        return value;
    }

    private static String removeUnderscore(String value) {
        return value.replaceAll("_", " ");
    }

    private static Map<String, String> setColumn(String key, String value) {
        Map<String, String> column = new HashMap<>();
        column.put("name", key);
        column.put("value", value);
        return column;
    }

    private static Map<String, String> setId() {
        Map<String, String> column = new HashMap<>();
        String uuidString = UUID.randomUUID().toString().replaceAll("-", "");
        column.put("name", "id");
        column.put("value", uuidString);
        return column;
    }

    private static EbiSearchEntry setCrossReferences(Set<String> modelIdsInRecord, EbiSearchEntry ebiSearchEntry) {
        for (String modelId : modelIdsInRecord) {
            Map<String, String> modelMap = new HashMap<>();
            modelMap.put("dbname", "BIOMODELS");
            modelMap.put("dbkey", modelId);
            ebiSearchEntry.addCrossReference(modelMap);
        }

        return ebiSearchEntry;

    }

    private static boolean isRAWField(String key) {
        return rawFieldNames.contains(key.toLowerCase(new Locale("en")));
    }

    private static boolean isKeepUnderscoreField(String key) {
        return keepUnderScoreFields.contains(key.toLowerCase(new Locale("en")));
    }

    public static EbiSearchJsonView fromCSVToEbiSearchJSON(CSVReaderHeaderAware csvReaderHeaderAware) throws IOException {
        Map<String, String> csvMap = csvReaderHeaderAware.readMap();
        EbiSearchJsonView ebiSearchJsonView = new EbiSearchJsonView();
        Set<String> modelIdsInRecord = new HashSet<>();

        while (csvMap != null) {
            EbiSearchEntry ebiSearchEntry = new EbiSearchEntry();

            // Traversing through all the fields in an entry
            for (Map.Entry<String, String> entry : csvMap.entrySet()) {

                String originalValue = entry.getValue().trim();
                String formattedKey = formatJsonKey(entry.getKey());

                //Adding RAW field as is
                if (isRAWField(formattedKey)) {
                    ebiSearchEntry.addField(setColumn(formattedKey + "_RAW", originalValue));
                }
                String value = formatJsonValue(originalValue);

                if (!isKeepUnderscoreField(formattedKey)) {
                    value = removeUnderscore(value);
                }
                //Adding formatted field
                String formattedValue = formatJsonValue(value);
                ebiSearchEntry.addField(setColumn(formattedKey,
                    value
                        .replaceAll("\"", "")
                        .trim()));

                if (formattedKey.equalsIgnoreCase("model")) {
                    modelIdsInRecord.add(formattedValue);
                }
            }

            // Adding Id field
            ebiSearchEntry.addField(setId());

            // Setting cross-references
            ebiSearchEntry = setCrossReferences(modelIdsInRecord, ebiSearchEntry);

            // Adding a single entry in list of entries
            ebiSearchJsonView.entries.add(ebiSearchEntry);
            csvMap = csvReaderHeaderAware.readMap();
        }
        ebiSearchJsonView.entryCount = ebiSearchJsonView.entries.size();
        return ebiSearchJsonView;
    }

    public String getName() {
        return name;
    }

    public String getRelease() {
        return release;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public long getEntryCount() {
        return entryCount;
    }

    public List<EbiSearchEntry> getEntries() {
        return entries;
    }

    public EbiSearchEntry getEntry(int index) {
        return entries.get(index);
    }
}

package net.biomodels.parameters.export;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.opencsv.CSVReaderHeaderAware;
import net.biomodels.parameters.DefaultParamsWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author carankalle on 26/09/2018.
 */
public class EbiSearchCSVToJSONExporter {

    public static void main(String[] args) {
        String sourcePath = args[0];
        try {
            if (sourcePath != null) {
                File source = new File(sourcePath);
                EbiSearchCSVToJSONExporter ebiSearchCsvToJSONExporter = new EbiSearchCSVToJSONExporter();
                if (!source.exists()) {
                    System.err.println("Error\tPlease provide source file name or path");
                    return;
                }
                if (source.isDirectory()) {
                    File[] files = source.listFiles();
                    if (files != null) {
                        for (File file : files) {
                            if (file.getName().endsWith("csv")) {
                                String sourceFileNameWithPath = sourcePath + "/" + file.getName();
                                ebiSearchCsvToJSONExporter.runCSVToJsonModule(sourceFileNameWithPath);
                            }
                        }
                    }
                } else {
                    ebiSearchCsvToJSONExporter.runCSVToJsonModule(sourcePath);
                }
            }

        } catch (IOException e) {
            System.err.printf("Error\tWhile converting file from csv to JSON " + e.getMessage());
        }

    }

    private String convertToJson(String sourceCSVFileName) throws IOException {

        CSVReaderHeaderAware csvReaderHeaderAware = new CSVReaderHeaderAware(
            new InputStreamReader(
                new FileInputStream(sourceCSVFileName), "UTF-8"));
        EbiSearchJsonView convertedEbiSearchJsonView = EbiSearchJsonView.fromCSVToEbiSearchJSON(csvReaderHeaderAware);
        Gson gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .disableHtmlEscaping()
            .setPrettyPrinting().create();
        return gson.toJson(convertedEbiSearchJsonView);
    }

    private String getOutputPathForJsonFile(String sourceFileNameWithPath) {
        File sourceFile = new File(sourceFileNameWithPath);
        int lastIndexOfSlash =
            sourceFileNameWithPath
                .lastIndexOf('/');
        return sourceFileNameWithPath.substring(0, lastIndexOfSlash) + "/" + sourceFile.getName() + ".json";
    }

    public void runCSVToJsonModule(String sourceFileNameWithPath) throws IOException {
        System.out.println("INFO\tProcessing  file: " + sourceFileNameWithPath);
        String outputPathForJsonFile = getOutputPathForJsonFile(sourceFileNameWithPath);
        String jsonData = this.convertToJson(sourceFileNameWithPath);
        File outputFile = new File(outputPathForJsonFile);
        DefaultParamsWriter defaultParamsWriter = new DefaultParamsWriter();
        defaultParamsWriter.writeToJSON(jsonData, outputFile);
        System.out.println("INFO\tSuccessfully converted file: " + sourceFileNameWithPath);
    }
}

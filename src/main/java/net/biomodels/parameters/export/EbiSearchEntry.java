package net.biomodels.parameters.export;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author carankalle on 14/11/2018.
 */
@SuppressWarnings("WeakerAccess")
public class EbiSearchEntry {
    private List<Map<String, String>> fields;
    private List<Map<String, String>> crossReferences;

    public EbiSearchEntry() {
        fields = new ArrayList<>();
        crossReferences = new ArrayList<>();
    }

    public List<Map<String, String>> getFields() {
        return fields;
    }

    public void addField(Map<String, String> field) {
        this.fields.add(field);
    }

    public Map<String, String> getField(int index) {
        return this.fields.get(index);
    }

    public void addCrossReference(Map<String, String> crossReference) {
        this.crossReferences.add(crossReference);
    }

    public Map<String, String> getCrossReferences(int index) {
        return this.crossReferences.get(index);
    }
 }

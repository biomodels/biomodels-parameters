package net.biomodels.parameters;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author carankalle on 19/07/2019.
 */
public class SabioRKExtractionService implements ExternalLinkExtractionService {

    @Override
    public Set<String> loadMappingFile() {
        InputStream in = getClass().getClassLoader().getResourceAsStream("sabiork_accessions.txt");

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(in, "UTF-8"))) {

            return reader.lines().parallel()
                .collect(Collectors.toSet());

        } catch (IOException ie) {
            System.err.printf("Unable to parse reactome mapping file, %s", ie.getMessage());
        }
        return null;
    }
}


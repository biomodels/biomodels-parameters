package net.biomodels.parameters;

import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.LocalParameter;

import java.util.List;
import java.util.Map;

/**
 * Created by carankalle on 30/08/2018.
 */
public interface GlobalParametersMatcher {

    String getMatchedGlobalParameters(ASTNode astNode, Map<String, String> globalParameters, List<LocalParameter> localParameters);

    void computeEquationsParameterString(List<ASTNode> astNodes, Map<String, String> globalParameters);

}

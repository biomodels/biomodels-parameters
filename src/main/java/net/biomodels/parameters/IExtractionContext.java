package net.biomodels.parameters;

import org.sbml.jsbml.Model;

import java.util.Map;
import java.util.Set;

public interface IExtractionContext {
    Set<String> getReactomeSet();

    Set<String> getSabioRKSet();

    Map<String, ReactionInfo> getReactionInfoMap();

    ReactionInfo getReactionInfo(String reactionId);

    SpeciesInformation[] getInformationForSpecies(String[] species);

    Map<String, String> getGlobalParameters();

    Map<String, SpeciesInformation> getGlobalSpeciesAnnotations();

    void extractReactionInfoMap(Model model);
}

package net.biomodels.parameters;

import net.biomodels.parameters.exceptions.CustomException;

/**
 * @author carankalle on 20/11/2018.
 */
public class ArgumentParser {

    public ArgumentParser() {
    }

    public RequestContext parseCliArgument(String[] ar, RequestContext requestContext) throws CustomException {

        if (ar.length == 0) {
            throw new CustomException("# Please enter source file/directory name & destination file name");
        }

        String[] arguments = ar[0].split(",");
        if (arguments.length <= 1) {
            throw new CustomException("# Please enter at least modelId from BiomdId, modelId and a source file name");

        }
        String modelId = arguments[0];
        String biomdId = arguments[1];
        String sourceFilename = arguments[2];

        if (null == sourceFilename) {
            throw new CustomException("Source file name is empty");
        } else if (null == modelId && null == biomdId) {
            throw new CustomException("ModelId and BiomdId are empty");
        }
        String destinationFilepath = ar[1];

        requestContext.setModelIdFromRequestContext(modelId);
        requestContext.setBiomdIdFromRequestContext(biomdId);
        requestContext.setSourceFileName(sourceFilename);
        requestContext.setDestinationFilePath(destinationFilepath);
        return requestContext;
    }
}

package net.biomodels.parameters;

import net.biomodels.parameters.exceptions.CustomException;
import net.biomodels.parameters.export.EbiSearchCSVToJSONExporter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

/**
 * This class implements a solution to {@link ExtractParamInterface} abstraction that
 * includes operations such as
 * getting a request context and default parameters,
 * extracting all available parameters from model files,
 * defining the main executable point.
 *
 * @author carankalle on 20/07/18.
 */
@SuppressWarnings("WeakerAccess")
public class ExtractParam implements ExtractParamInterface {

    private DefaultParamsWriter defaultParamsWriter;
    private RequestContext requestContext;

    public ExtractParam() throws SQLException {
        requestContext = new RequestContext();
        defaultParamsWriter = new DefaultParamsWriter();
    }

    public ExtractParam(RequestContext requestContext) {
        this.requestContext = requestContext;
        defaultParamsWriter = new DefaultParamsWriter();
    }

    public static void main(String ar[]) throws SQLException, CustomException, IOException {
        ExtractParam ext = null;
        try {
            ext = new ExtractParam();
            ext.runModule(ar);
        } finally {
            if (null != ext) {
                ext.requestContext.close();
            }
        }
    }

    public RequestContext getRequestContext() {
        return requestContext;
    }

    public DefaultParamsWriter getDefaultParamsWriter() {
        return defaultParamsWriter;
    }

    private File getOutputLocationForInputFile(File input, String outDirectoryPath) throws IOException {
        boolean invalidInput = null == input || !input.exists();
        if (invalidInput) {
            throw new IOException("Model file must exist, this file does not exist " + input);
        }
        boolean invalidOutput = outDirectoryPath == null || !(new File(outDirectoryPath).exists());
        if (invalidOutput) {
            throw new IOException("Output folder path must exist, this path does not exist " + outDirectoryPath);
        }
        String outputName = input.getName() + ".csv";
        return new File(outDirectoryPath, outputName);
    }

    @Override
    public void runModule(String ar[]) throws CustomException, IOException {

        ArgumentParser argumentParser = new ArgumentParser();
        requestContext = argumentParser.parseCliArgument(ar, requestContext);
        String sourceFileName = requestContext.getSourceFileName();
        String destinationFilePath = requestContext.getDestinationFilePath();

        File sourceFile = new File(sourceFileName);

        if (!sourceFile.exists()) {
            throw new FileNotFoundException(sourceFile.getAbsolutePath() + " file not found");
        }
        extract(sourceFile, destinationFilePath);
    }

    @Override
    public void extract(File fileItem, String destinationFilepath) throws IOException {
        ExtractionService extractionService = new ExtractionService(requestContext);
        List<ParamsModel> paramsFromModelFile = extractionService.getParamsFromModelFile(fileItem);

        if (null == paramsFromModelFile) {
            return;
        }

        File outFile = getOutputLocationForInputFile(fileItem, destinationFilepath);
        defaultParamsWriter.writeToCSV(paramsFromModelFile, outFile);
        new EbiSearchCSVToJSONExporter()
            .runCSVToJsonModule(destinationFilepath + "/" + outFile.getName());
    }
}

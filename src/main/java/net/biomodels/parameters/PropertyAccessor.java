package net.biomodels.parameters;

import net.biomodels.parameters.exceptions.PropertiesNotFound;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by carankalle on 17/09/2018.
 */
public class PropertyAccessor {

    private Properties prop = new Properties();


    public PropertyAccessor(String propertiesPath) {
        FileInputStream input = null;
        try {
            input = new FileInputStream(propertiesPath);
            prop.load(input);

        } catch (IOException ie) {
            throw new PropertiesNotFound("\nIOException: Unable to read/find properties file FilePath= " + propertiesPath + " Message= " + ie.getMessage());
        } catch (NullPointerException ne) {
            throw new PropertiesNotFound("\nNullPointerException: Unable to read/find properties file FilePath= " + propertiesPath + " Message= " + ne.getMessage());
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            } catch (IOException ie) {
                System.err.println("Error\twhile closing file inputstream");
            }
        }
    }

    public String getProperty(String key) {
        String value = prop.getProperty(key);
        if (value != null && !value.isEmpty()) {
            return value;
        } else {
            throw new PropertiesNotFound("Could not find property " + key);
        }
    }
}

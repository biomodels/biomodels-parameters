package net.biomodels.parameters;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by carankalle on 26/09/2018.
 */
public interface ParamsWriter {

    public void writeToCSV(List<ParamsModel> paramsModels, File outputFile) throws IOException;

    public void writeToJSON(String jsonData, File outputFile) throws IOException;

}

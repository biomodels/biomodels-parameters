package net.biomodels.parameters;

import net.biomodels.parameters.dao.DaoController;

import java.sql.SQLException;

/**
 * @author carankalle on 20/11/2018.
 */
@SuppressWarnings("WeakerAccess")
public class RequestContext {
    private final DaoController daoController;
    private String sourceFileName;
    private String destinationFilePath;
    private String modelIdFromRequestContext, biomdIdFromRequestContext;

    public RequestContext(DaoController daoController) {
        this.daoController = daoController;
    }

    public RequestContext() throws SQLException {
        this.daoController = new DaoController();
    }

    public void close() {
        try {
            if (null != this.getDaoController()) {
                this.getDaoController().close();
            }
        } catch (Exception e) {
            System.err.printf("Error\t: Could not close database connection pool: %s %n", e.getMessage());
        }
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }

    public String getDestinationFilePath() {
        return destinationFilePath;
    }

    public void setDestinationFilePath(String destinationFilePath) {
        this.destinationFilePath = destinationFilePath;
    }

    public DaoController getDaoController() {
        return daoController;
    }

    public String getModelIdFromRequestContext() {
        return modelIdFromRequestContext;
    }

    public void setModelIdFromRequestContext(String modelIdFromRequestContext) {
        this.modelIdFromRequestContext = modelIdFromRequestContext;
    }

    public String getBiomdIdFromRequestContext() {
        return biomdIdFromRequestContext;
    }

    public void setBiomdIdFromRequestContext(String biomdIdFromRequestContext) {
        this.biomdIdFromRequestContext = biomdIdFromRequestContext;
    }
}

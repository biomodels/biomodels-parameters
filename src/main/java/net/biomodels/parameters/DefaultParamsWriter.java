package net.biomodels.parameters;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author carankalle on 26/09/2018.
 */
public class DefaultParamsWriter implements ParamsWriter {

    public void writeToCSV(List<ParamsModel> globalParamsmodelList, File outputFile) throws IOException {

        String NEW_LINE_SEPARATOR = "\n";

        if (globalParamsmodelList.isEmpty()) {
            System.out.printf("Warning\tNo records to write to file %s", outputFile);
        }
        final String FILE_HEADER =
            "Entity," +
                "Entity Id," +
                "Entity Accession Url," +
                "Reaction," +
                "Reaction Original," +
                "Model," +
                "Organism," +
                "Publication," +
                "Parameters," +
                "Rate," +
                "Rate Original," +
                "Assignment," +
                "Assignment Original," +
                "Initial Data," +
                "Reactants," +
                "Products," +
                "Modifiers," +
                "Reaction SBO Term," +
                "Reaction SBO Term Link," +
                "Entity SBO Term," +
                "Entity SBO Term Link," +
                "External links," +
                "Is Curated";
        List<SearchView> searchViews = globalParamsmodelList
            .stream()
            .map(SearchView::convertParamsModelToSearchView)
            .filter(Objects::nonNull)
            .collect(Collectors.toList());

        try (Writer writer = new OutputStreamWriter(new FileOutputStream(outputFile), "UTF-8");
             BufferedWriter bw = new BufferedWriter(
                 new PrintWriter(writer, true))) {

            // Appending HEADER
            bw.append(FILE_HEADER);
            bw.append(NEW_LINE_SEPARATOR);

            //Write a new SearchView object list to the CSV file
            for (SearchView searchView : searchViews) {
                List<Object> fields = Arrays.asList(
                    searchView.getEntity(),
                    searchView.getEntityId(),
                    searchView.getEntityAccessionUrlString(),
                    searchView.getReaction(),
                    searchView.getReactionOriginal(),
                    searchView.getModel(),
                    searchView.getOrganism(),
                    searchView.getPublication(),
                    searchView.getParameters(),
                    searchView.getRate(),
                    searchView.getRateOriginal(),
                    searchView.getAssignment(),
                    searchView.getAssignmentOriginal(),
                    searchView.getInitialData(),
                    searchView.getReactants(),
                    searchView.getProducts(),
                    searchView.getModifiers(),
                    searchView.getReactionSBOTerm(),
                    searchView.getReactionSBOTermUrlString(),
                    searchView.getEntitySBOTerm(),
                    searchView.getEntitySBOTermUrlString(),
                    searchView.getExternalLinks(),
                    searchView.getIsCurated());
                String row = fields
                    .stream()
                    .map(this::preFormatField)
                    .collect(Collectors.joining(","));
                bw.append(row);
                bw.newLine();
            }
            System.out.println("CSV file was created successfully !!!");
        }
    }

    @Override
    public void writeToJSON(String jsonData, File outputFile) throws IOException {
        if (jsonData.length() == 0) {
            System.out.printf("Warning\tNo records in Json data to write to file %s", outputFile);
        }
        try (PrintStream out = new PrintStream(outputFile, "UTF-8")) {
            out.println(jsonData);
        }
    }

    /**
     * This method deals with every field
     * and convert it to String.
     *
     * @param object input Object instance
     * @return {@link String}
     */

    private String preFormatField(Object object) {
        if (null == object) {
            return "\"\"";
        }
        return "\"" + object.toString() + "\"";
    }


}

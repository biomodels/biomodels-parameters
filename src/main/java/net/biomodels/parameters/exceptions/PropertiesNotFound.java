package net.biomodels.parameters.exceptions;

/**
 * Created by carankalle on 27/07/2018.
 */
public class PropertiesNotFound extends RuntimeException {


    public PropertiesNotFound(String s) {
        // Call constructor of parent Exception
        super(s);
    }
}



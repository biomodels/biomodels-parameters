package net.biomodels.parameters.exceptions;

/**
 * Created by carankalle on 27/07/2018.
 */
public class CustomException extends Exception {


    public CustomException(String s)
    {
        // Call constructor of parent Exception
        super(s);
    }
}



package net.biomodels.parameters.exceptions;

/**
 * Created by carankalle on 14/08/2018.
 */
public class SpeciesNotFoundException extends Exception {


    public SpeciesNotFoundException(String s)
    {
        // Call constructor of parent Exception
        super(s);
    }
}



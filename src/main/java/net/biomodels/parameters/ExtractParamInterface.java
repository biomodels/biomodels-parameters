package net.biomodels.parameters;

import net.biomodels.parameters.exceptions.CustomException;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

/**
 * @author carankalle on 20/07/18.
 */
public interface ExtractParamInterface {
    /**
     * Extracts all parameters from the input file and saved them into the output file
     *
     * @param fileItem  A {@link File} object indicating the input source
     * @param destinationFilepath A {@link File} object indicating the output place
     * @throws IOException
     */
    void extract(File fileItem, String destinationFilepath) throws IOException;

    void runModule(String ar[]) throws CustomException, IOException, SQLException;

}

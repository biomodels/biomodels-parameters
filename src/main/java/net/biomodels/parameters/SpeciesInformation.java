package net.biomodels.parameters;

import net.biomodels.parameters.dao.ResourceReference;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author carankalle on 17/08/2018.
 */
@SuppressWarnings("WeakerAccess")
public class SpeciesInformation {
    private final static String SEMICOLON = "; ";
    public static final char LEGEND_FIELD_SEPARATOR = '$';

    private List<ResourceReference> resourceReferences;
    private String speciesAnnotationString;
    private String annotationExternalLinkString = "";
    private String speciesIdForGlobalAnnotations = "";
    private String speciesName = "";
    private String accessionUrlString = "";
    private Map<String,String> sabioRKQueryPrefixMap = new HashMap<>();

    public SpeciesInformation(List<ResourceReference> references, Set<String> reactomeSet, Set<String> sabioRKSet) {
        sabioRKQueryPrefixMap.put("kegg","KeggCompoundID:");
        sabioRKQueryPrefixMap.put("chebi","ChebiID:");
        sabioRKQueryPrefixMap.put("uniprot","UniProtKB_AC:");
        this.resourceReferences = Objects.requireNonNull(references);
        computeAnnotationString();
        computeAccessionUrlString();
        if (!reactomeSet.isEmpty() && !sabioRKSet.isEmpty()) {
            computeAnnotationExternalLinks(reactomeSet, sabioRKSet);
        }
    }

    public String getAccessionUrlString() {
        return accessionUrlString;
    }

    public String getAnnotationExternalLinkString() {
        return annotationExternalLinkString;
    }

    public String getSpeciesIdForGlobalAnnotations() {
        return speciesIdForGlobalAnnotations;
    }

    public void setSpeciesIdForGlobalAnnotations(String speciesIdForGlobalAnnotations) {
        this.speciesIdForGlobalAnnotations = speciesIdForGlobalAnnotations;
    }

    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName == null ? "" : speciesName;
    }

    private void computeAnnotationExternalLinks(Set<String> reactomeSet, Set<String> sabioRKSet) {
        String reactomeLinks = getExternalLinks(reactomeSet, "reactome:");
        String sabioRKLinks = getExternalLinks(sabioRKSet, "sabiork.compound:");
        annotationExternalLinkString = (!sabioRKLinks.isEmpty() ? sabioRKLinks : "") +
            (!sabioRKLinks.isEmpty() && !reactomeLinks.isEmpty() ? SEMICOLON : "") +
            (!reactomeLinks.isEmpty() ? reactomeLinks : "");
    }

    private String getMatchingPrefixForSabioRKQuery(String accession) {
        if (null == accession) {
            return null;
        }
        String sabioRKQueryPrefix = sabioRKQueryPrefixMap.get(accession.split(":")[0].toLowerCase(Locale.ENGLISH));
        return null == sabioRKQueryPrefix ? "" : sabioRKQueryPrefix;
    }

    private String getExternalLinks(Set<String> idSet, String prefix) {
        return resourceReferences.stream()
            .map(resourceReference -> {
                final String accession = resourceReference.getAccession();
                String accessionWithoutPrefix =
                    extractValueFromAccessionWithColon(accession);
                if (idSet.contains(accessionWithoutPrefix)) {
                    if (prefix.contains("sabiork")) {
                        accessionWithoutPrefix = getMatchingPrefixForSabioRKQuery(accession) + accessionWithoutPrefix;
                    }
                    return prefix + accessionWithoutPrefix;
                }
                return null;
            })
            .filter(Objects::nonNull)
            .collect(Collectors.joining(SEMICOLON));
    }

    private void computeAnnotationString() {
        String jointReferences = resourceReferences.stream()
            .map(ResourceReference::getFormattedValue)
            .collect(Collectors.joining(SEMICOLON));
        speciesAnnotationString = !jointReferences.isEmpty() ? "[" + jointReferences + "]" : "";
    }


    private void computeAccessionUrlString() {
        String newAccessionUrlString = resourceReferences
            .stream()
            .map(resourceReference -> {
                String uri = resourceReference.getUri();
                String accessionOrUrl;
                if (null == uri) {
                    accessionOrUrl = resourceReference.getAccession();
                } else {
                    accessionOrUrl = this.prepareAccessionResolvedTermUrl(uri, resourceReference.getName());
                }
                return accessionOrUrl;
            })
            .collect(Collectors.joining(SEMICOLON));

        if (null == newAccessionUrlString || newAccessionUrlString.equalsIgnoreCase("null")) {
            accessionUrlString = "";
        } else {
            accessionUrlString = newAccessionUrlString;
        }
    }


    private String prepareAccessionResolvedTermUrl(String resource, String label) {
        if (null != resource) {
            return resource + "|" + label;
        }
        return null;
    }

    public String getSpeciesAnnotationString() {
        return speciesAnnotationString;
    }

    private String extractValueFromAccessionWithColon(String accession) {
        if (null == accession) {
            return null;
        }
        int indexOfColon = accession.indexOf(':');
        if (indexOfColon != -1) {
            return accession.substring(indexOfColon + 1);
        } else {
            return accession;
        }
    }

    public String asSpeciesLegendSummary() {
        return speciesIdForGlobalAnnotations +
            LEGEND_FIELD_SEPARATOR +
            speciesName +
            LEGEND_FIELD_SEPARATOR +
            accessionUrlString;
    }
}

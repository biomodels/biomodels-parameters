package net.biomodels.parameters;

import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.LocalParameter;
import org.sbml.jsbml.UnitDefinition;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author carankalle on 30/08/2018.
 */
public class DefaultGlobalParametersMatch implements GlobalParametersMatcher {

    private List<String> parameters = new ArrayList<>();
    private final static String SEMICOLON = "; ";


    public String getMatchedGlobalParameters(ASTNode astNode, Map<String, String> globalParameters, List<LocalParameter> localParameters) {
        this.parameters = new ArrayList<>();
        if (localParameters != null && !localParameters.isEmpty()) {
            for (LocalParameter localParameter : localParameters) {
                final UnitDefinition unitsInstance = localParameter.getUnitsInstance();
                String unitString = unitsInstance != null ? " ".concat(unitsInstance.toString()) : "";
                String localParameterId = localParameter.getId();
                String localParameterValue = localParameter.getValue() + unitString;
                this.parameters.add(localParameterId + "=" + localParameterValue);
            }
        }
        List<ASTNode> astNodes = astNode.getChildren();
        this.computeEquationsParameterString(astNodes, globalParameters);
        parameters = new ArrayList<>(new HashSet<>(parameters));
        return parameters.stream().collect(Collectors.joining(SEMICOLON));
    }


    public void computeEquationsParameterString(List<ASTNode> astNodes, Map<String, String> globalParameters) {
        for (ASTNode astNode : astNodes) {
            if (astNode.getNumChildren() != 0) {
                computeEquationsParameterString(astNode.getChildren(), globalParameters);
            } else {
                String parameter = astNode.toString();
                if (globalParameters.containsKey(parameter)) {
                    this.parameters.add(parameter + " = " + globalParameters.get(parameter));
                }
            }
        }

    }

}

package net.biomodels.parameters;

import org.sbml.jsbml.Model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author carankalle on 15/08/2018.
 */
public class ExtractionContext implements IExtractionContext {
    private Map<String, SpeciesInformation> globalSpeciesAnnotations = new HashMap<>();
    private Map<String, ReactionInfo> reactionInfoMap; // set in extractReactionInfoMap
    private Map<String, String> globalParameters = new HashMap<>();
    private Set<String> reactomeSet = new HashSet<>();
    private Set<String> sabioRKSet = new HashSet<>();

    @Override
    public Set<String> getReactomeSet() {
        return reactomeSet;
    }

    @Override
    public Set<String> getSabioRKSet() {
        return sabioRKSet;
    }

    @Override
    public Map<String, ReactionInfo> getReactionInfoMap() {
        return reactionInfoMap;
    }

    @Override
    public ReactionInfo getReactionInfo(String reactionId) {
        return Objects.requireNonNull(reactionInfoMap, "was extractReactionInfoMap() called?")
            .get(reactionId);
    }

    @Override
    public SpeciesInformation[] getInformationForSpecies(String[] species) {
        return Arrays.stream(species)
            .map(globalSpeciesAnnotations::get)
            .filter(Objects::nonNull)
            .collect(Collectors.toList()).toArray(new SpeciesInformation[]{});
    }

    public void loadReactomeMappingFileFromExtractionContext() {
        ExternalLinkExtractionService reactomeExtractionService = new ReactomeExtractionService();
        reactomeSet = reactomeExtractionService.loadMappingFile();
    }

    public void loadSabioRKMappingFileFromExtractionContext() {
        ExternalLinkExtractionService sabioRKExtractionService = new SabioRKExtractionService();
        sabioRKSet = sabioRKExtractionService.loadMappingFile();
    }

    @Override
    public Map<String, String> getGlobalParameters() {
        return globalParameters;
    }

    public void setGlobalParameters(Map<String, String> globalParameters) {
        this.globalParameters = globalParameters;
    }

    public void setGlobalSpeciesAnnotations(Map<String, SpeciesInformation> globalSpeciesAnnotations) {
        this.globalSpeciesAnnotations = globalSpeciesAnnotations;
    }

    @Override
    public Map<String, SpeciesInformation> getGlobalSpeciesAnnotations() {
        return globalSpeciesAnnotations;
    }

    @Override
    public void extractReactionInfoMap(Model model) {
        this.reactionInfoMap = Objects.requireNonNull(model, "Cannot parse a null model")
            .getListOfReactions()
            .stream()
            .map(reaction -> ReactionInfo.parse(reaction, this))
            .collect(Collectors.toMap(ReactionInfo::getId, reactionInfo -> reactionInfo));
    }
}

package net.biomodels.parameters;

import net.biomodels.parameters.dao.ResourceReference;
import net.biomodels.parameters.exceptions.CustomException;
import net.biomodels.parameters.exceptions.SpeciesNotFoundException;
import org.sbml.jsbml.CVTerm;
import org.sbml.jsbml.ExplicitRule;
import org.sbml.jsbml.KineticLaw;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Parameter;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.Rule;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.Unit;
import org.sbml.jsbml.UnitDefinition;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author carankalle on 23/11/2018.
 */
@SuppressWarnings("WeakerAccess")
public class ExtractionService {

    private final static String SEMICOLON = "; ";
    public static final List<String> BLACKLISTED_SPECIES = Collections.unmodifiableList(Arrays.asList("EmptySet", "null"));
    private DefaultGlobalParametersMatch defaultGlobalParametersMatch = new DefaultGlobalParametersMatch();
    private RequestContext requestContext;
    private boolean shouldResolveAnnotations;
    private ExtractionContext extractionContext = new ExtractionContext();

    public ExtractionService(RequestContext requestContext) {
        this.requestContext = requestContext;
        shouldResolveAnnotations = requestContext.getDaoController() != null;

    }

    @SuppressWarnings("unused")
    public RequestContext getRequestContext() {
        return requestContext;
    }

    @SuppressWarnings("unused")
    public void setRequestContext(RequestContext requestContext) {
        this.requestContext = requestContext;
    }

    public IExtractionContext getExtractionContext() {
        return extractionContext;
    }

    public String resolve(String accession) {
        if (null == accession || accession.isEmpty()) {
            return accession;
        }
        ResourceReference resourceReference = new ResourceReference();
        try {
            resourceReference = getMatchingResourceReferenceForAccession(accession);
        } catch (SQLException e) {
            System.err.println("Error\t" + "Unable to resolve" + e.getMessage());
        }
        if (resourceReference == null) {
            System.err.println("Error\t" + "No records found for accession " + accession);
        } else if (resourceReference.getName() == null) {
            System.err.println("Error\t" + "No name associated with the Term " + accession);
            return resourceReference.getFormattedValue();
        } else {
            return resourceReference.getFormattedValue();
        }

        return accession;
    }

    private ResourceReference getMatchingResourceReferenceForAccession(String accession) throws SQLException {
        return requestContext.getDaoController().getReference(accession);
    }

    public Model getModel(File file) throws IOException, XMLStreamException {
        SBMLDocument sbmldocument = new SBMLReader().readSBML(file);
        return Objects.requireNonNull(sbmldocument, "JSBML could not parse " + file)
            .getModel();
    }

    public void initializeExtractionContext(Model jsbml) {
        extractionContext.loadReactomeMappingFileFromExtractionContext();
        extractionContext.loadSabioRKMappingFileFromExtractionContext();
        extractionContext.setGlobalSpeciesAnnotations(getAnnotationsFromSpecies(jsbml, extractionContext));
        extractionContext.setGlobalParameters(getGlobalParametersInfo(jsbml));
        extractionContext.extractReactionInfoMap(jsbml);
    }

    private Map<String, String> getGlobalParametersInfo(Model jsbmlModel) {
        Map<String, String> globalParameters = new HashMap<>();
        for (Parameter parameter : jsbmlModel.getListOfParameters()) {
            final UnitDefinition unitDefinition = parameter.getDerivedUnitDefinition();
            String unitString = unitDefinition != null ? " ".concat(unitDefinition.toString()) : "";
            globalParameters.put(parameter.getId(), String.valueOf(parameter.getValue()) + unitString);
        }
        return globalParameters;
    }


    /**
     * Retrieves the resolved resource references from a list of CVTerms.
     *
     * @param cvTerms the {@link List<CVTerm>} which contains the list of CVTerms associated with an annotation
     * @return Resolved resourceReferences {@link List<ResourceReference>}
     */
    private List<ResourceReference> getResourceReferencesFromCVTerms(List<CVTerm> cvTerms) {

        List<ResourceReference> resourceReferences = new ArrayList<>();
        String accession;

        for (CVTerm cvterm : cvTerms) {

            for (String resource : cvterm.getResources()) {
                accession = resource.substring(resource.lastIndexOf("/") + 1);
                ResourceReference resourceReference = null;
                if (shouldResolveAnnotations) {
                    try {
                        resourceReference = getMatchingResourceReferenceForAccession(accession);
                    } catch (SQLException e) {
                        System.err.println("Error\t" + "Unable to resolve" + e.getMessage());
                    }
                }
                if (resourceReference == null || resourceReference.getName() == null) {
                    System.err.println("Error\t" + "No records found for accession " + accession);
                    ResourceReference unresolved = new ResourceReference();
                    unresolved.setAccession(accession);
                    resourceReferences.add(unresolved);
                } else {
                    resourceReferences.add(resourceReference);
                }

            }

        }
        return resourceReferences;
    }

    public Map<String, SpeciesInformation> getAnnotationsFromSpecies(Model jsbml, IExtractionContext extractionContext) {

        Map<String, SpeciesInformation> finalMap = new HashMap<>();
        for (Species tempSpecies : jsbml.getListOfSpecies()) {

            String speciesId = tempSpecies.getId();
            SpeciesInformation speciesInformation;
            if (!tempSpecies.getAnnotation().isEmpty()) {
                List<ResourceReference> resourceReferencesForAnnotations =
                    this.getResourceReferencesFromCVTerms(tempSpecies.getAnnotation().getListOfCVTerms());
                speciesInformation = new SpeciesInformation(resourceReferencesForAnnotations,
                    extractionContext.getReactomeSet(),
                    extractionContext.getSabioRKSet());
                speciesInformation.setSpeciesIdForGlobalAnnotations(speciesId);
                speciesInformation.setSpeciesName(tempSpecies.getName());
                finalMap.put(tempSpecies.getId(), speciesInformation);
            } else {
                speciesInformation = new SpeciesInformation(new ArrayList<>(), new HashSet<>(), new HashSet<>());
                speciesInformation.setSpeciesIdForGlobalAnnotations(speciesId);
                speciesInformation.setSpeciesName(tempSpecies.getName());
                finalMap.put(tempSpecies.getId(), speciesInformation);
            }
        }
        return finalMap;

    }

    private String prepareSboResolvedTermLink(String sboTerm, String label) {
        if (null == sboTerm || label.isEmpty()) {
            return "";
        }
        // label is never empty or null, but could be the accession itself
        return "https://identifiers.org/sbo/" +
            sboTerm +
            "|" +
            label;
    }

    public List<ParamsModel> extractAllParameters(File sourceSBMLfile)
        throws CustomException, IOException, XMLStreamException, SpeciesNotFoundException {

        Model jsbmlModel;
        ParamsModel paramsModel = new ParamsModel();
        List<ParamsModel> globalParamsModelList = new ArrayList<>();

        jsbmlModel = this.getModel(sourceSBMLfile);

        if (jsbmlModel.getNumSpecies() == 0) {
            throw new SpeciesNotFoundException("No Species in model " + sourceSBMLfile.getAbsolutePath());
        }

        this.initializeExtractionContext(jsbmlModel);
        paramsModel = this.extractModelInfo(paramsModel, jsbmlModel);
        paramsModel = this.extractOrganism(paramsModel, jsbmlModel);

        if (this.extractionContext.getGlobalSpeciesAnnotations().isEmpty()) {
            throw new CustomException("Unable to get annotations from Model " + sourceSBMLfile.getAbsolutePath());
        }
        for (Species species : jsbmlModel.getListOfSpecies()) {
            try {
                if (BLACKLISTED_SPECIES.contains(species.getId())) continue;

                paramsModel = this.extractSpeciesInfo(species, paramsModel);
                paramsModel = this.extractRefPublicationInfo(paramsModel, jsbmlModel);
                globalParamsModelList.addAll(this.extractReactionInfo(paramsModel, jsbmlModel, species));
                ParamsModel paramsModelForRule = this.extractRuleInfo(paramsModel, jsbmlModel, species);
                if (paramsModelForRule.getShow())
                    globalParamsModelList.add(paramsModelForRule);
            } catch (Exception ex) {
                System.err.printf("Error\tFailed to convert Species %s to ParamsModel: %s%n", species.getId(), ex.toString());
            }
        }
        return globalParamsModelList;
    }

    /**
     * This method serves the extraction of all parameters of the model given via a model file
     *
     * @param modelFile A {@link File} object
     * @return A {@link List} of {@link ParamsModel} composite objects
     */
    public List<ParamsModel> getParamsFromModelFile(File modelFile) {
        List<ParamsModel> allParams = null;
        if (modelFile.getName().endsWith("xml")) {
            System.out.println("\t # Processing " + modelFile.getName() + " file");
            try {
                allParams = this.extractAllParameters(modelFile);
            } catch (XMLStreamException e) {
                System.err.printf("Error\tError while extracting parameter info from %s: %s%n", modelFile, e);
            } catch (CustomException | IOException | SpeciesNotFoundException e) {
                System.err.printf("Error\tCould not process %s: %s%n", modelFile, e);
            }
        } else {
            System.err.println("Error\t" + "# " + modelFile.getAbsolutePath()
                + " not a valid file %n # File type not supported, "
                + "input should be XML and output should be CSV \n");
        }
        return allParams;
    }

    private ParamsModel extractRefPublicationInfo(ParamsModel paramsModel, Model jsbmlModel) {

        StringBuilder tempResources = new StringBuilder();
        for (CVTerm cvterm : jsbmlModel.getAnnotation().filterCVTerms(CVTerm.Qualifier.getModelQualifierFor("isDescribedBy"))) {
            for (String resource : cvterm.getResources()) {
                if (null == resource) {
                    System.err.println("Unable to add resource, resource is null");
                }
                tempResources.append(SEMICOLON).append(resource);
            }
        }

        String referencePublication = "";
        if (!tempResources.toString().isEmpty()) {
            referencePublication = tempResources.delete(0, 2).toString();
        }
        paramsModel.setReferencePublication(referencePublication);

        return paramsModel;

    }

    public ParamsModel extractOrganism(ParamsModel paramsModel, Model jsbmlModel) {

        List<ResourceReference> resourceReferencesForOrganisms =
            this.getResourceReferencesFromCVTerms(jsbmlModel.getAnnotation()
                .filterCVTerms(CVTerm.Qualifier.getBiologicalQualifierFor("hasTaxon")));
        String organismsString = resourceReferencesForOrganisms
            .stream()
            .map(resourceReference -> resolve(resourceReference.getAccession()))
            .collect(Collectors.joining(SEMICOLON));
        paramsModel.setOrganism(organismsString);

        return paramsModel;
    }

    @SuppressWarnings("unused")
    private ParamsModel extractModelInfo(ParamsModel paramsModel, Model jsbmlModel) {

        final String biomdId = requestContext.getBiomdIdFromRequestContext();
        final String modelId = requestContext.getModelIdFromRequestContext();
        String modelIdForParamsModel;
        boolean isCurated = true;
        modelIdForParamsModel = biomdId;

        if (null == biomdId || biomdId.isEmpty()) {
            modelIdForParamsModel = modelId;
            isCurated = false;
        }

        paramsModel.setModelID(modelIdForParamsModel);
        paramsModel.setIsCurated(isCurated);

        return paramsModel;


    }

    public ParamsModel extractSpeciesInfo(Species species, ParamsModel paramsModel) {
        paramsModel.setSpeciesId(species.getId());
        if (species.isSetName()) {
            paramsModel.setSpeciesName(species.getName());
        }
        paramsModel.setSpeciesMetaID(species.getMetaId());
        final UnitDefinition derivedSubstanceUnitDefinition = species.getDerivedSubstanceUnitDefinition();
        String units = "";
        if (null != derivedSubstanceUnitDefinition) {
            units = derivedSubstanceUnitDefinition.getListOfUnits().stream()
                .map(Unit::printUnit)
                .collect(Collectors.joining(" "));
        } else {
            System.err.printf("Error UnitDefinition is null for Species MetaId: %s %n", species.getMetaId());
        }

        if (species.isSetInitialAmount()) {
            paramsModel.setInitialData(String.valueOf(species.getInitialAmount()).concat(" ").concat(units));
        } else if (species.isSetInitialConcentration()) {
            paramsModel.setInitialData(String.valueOf(species.getInitialConcentration()).concat(" ").concat(units));
        }
        String resolvedSboTerm = this.resolve(species.getSBOTermID());
        String sboTermLink = this.prepareSboResolvedTermLink(species.getSBOTermID(), resolvedSboTerm);
        paramsModel.setSboTerm(resolvedSboTerm);
        paramsModel.setSpeciesSboTermUrlString(sboTermLink);
        paramsModel.setSpeciesAnnotation(this.extractionContext.getGlobalSpeciesAnnotations().get(species.getId()).getSpeciesAnnotationString());
        paramsModel.setAccessionUrlString(this.extractionContext.getGlobalSpeciesAnnotations().get(species.getId()).getAccessionUrlString());
        paramsModel.setExternalLinks(this.extractionContext.getGlobalSpeciesAnnotations().get(species.getId()).getAnnotationExternalLinkString());
        return paramsModel;
    }

    private List<ParamsModel> extractReactionInfo(ParamsModel paramsModel, Model jsbmlModel, Species species) {

        List<ParamsModel> paramsModelsList = new ArrayList<>();

        //# Iterating through all the reactions
        for (Reaction reaction : jsbmlModel.getListOfReactions()) {
            final KineticLaw kineticLaw = reaction.getKineticLaw();
            String parameters = "";
            if (null != kineticLaw) {
                parameters = this.defaultGlobalParametersMatch
                    .getMatchedGlobalParameters(
                        kineticLaw.getMath(),
                        this.extractionContext.getGlobalParameters(),
                        kineticLaw.getListOfLocalParameters());
            } else {
                System.err.printf("Error KineticLaw is null for Species MetaId: %s %n", species.getMetaId());
            }
            // Cloning the ParamsModel object
            ParamsModel clone;

            try {
                clone = (ParamsModel) paramsModel.clone();
            } catch (CloneNotSupportedException e) {
                System.err.println("Error\t" + "Could not clone " + paramsModel);
                continue;
            }

            //# Checking if reaction has species reference
            if (reaction.hasProduct(species) || reaction.hasReactant(species)) {
                final String reactionId = reaction.getId();
                ReactionInfo reactionInfo = extractionContext.getReactionInfo(reactionId);
                clone.setReactionID(reactionId);
                clone.setReactionInfo(reactionInfo);
                String resolvedSboTerm = this.resolve(reaction.getSBOTermID());
                String sboTermLink = this.prepareSboResolvedTermLink(
                    reaction.getSBOTermID(),
                    resolvedSboTerm);
                clone.setReactionSBOTerm(resolvedSboTerm);
                clone.setReactionSBOTermUrlString(sboTermLink);
                clone.setShow(true);
                clone.setParameters(parameters);
                paramsModelsList.add(clone);
            }
        }
        return paramsModelsList;
    }

    private ParamsModel extractRuleInfo(ParamsModel paramsModel, Model jsbmlModel, Species species) {
        ParamsModel clone;
        try {
            clone = (ParamsModel) paramsModel.clone();
        } catch (CloneNotSupportedException e) {
            System.err.println("Error\t" + "Could not clone " + paramsModel);
            return paramsModel;
        }
        for (Rule rule : jsbmlModel.getListOfRules()) {
            String formula = rule.getMath().toFormula();

            if (rule instanceof ExplicitRule) {
                String variable = ((ExplicitRule) rule).getVariable();
                if (variable != null &&
                    variable.equals(species.getId())) {
                    ReactionInfo reactionInfo = ReactionInfo.parse((ExplicitRule) rule, extractionContext);
                    if (reactionInfo == null) {
                        System.out.printf("Rule %s (%s=%s) returned a null reactionInfo%n", rule.getMetaId(), variable, formula);
                        return paramsModel;
                    }
                    clone.setReactionInfo(reactionInfo);
                    String parameters = defaultGlobalParametersMatch.getMatchedGlobalParameters(rule.getMath(),
                            this.extractionContext.getGlobalParameters(), null);
                    if (rule.getElementName().equals("rateRule")) {
                        clone.setRateRule(formula);
                        clone.setRateRuleOriginal(formula);
                        clone.setShow(true);
                        clone.setParameters(parameters);
                    }
                    if (rule.getElementName().equals("assignmentRule")) {
                        clone.setAssignmentRule(formula);
                        clone.setAssignmentRuleOriginal(formula);
                        clone.setShow(true);
                        clone.setParameters(parameters);
                    }
                }
            }
        }
        return clone;
    }
}

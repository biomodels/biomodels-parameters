package net.biomodels.parameters;

import java.util.Set;

/**
 * @author carankalle on 19/07/2019.
 */
public interface ExternalLinkExtractionService {
    Set<String> loadMappingFile();
}

package net.biomodels.parameters;

import org.sbml.jsbml.ExplicitRule;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Reaction;
import org.sbml.jsbml.SimpleSpeciesReference;
import org.sbml.jsbml.Species;
import org.sbml.jsbml.Variable;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

@SuppressWarnings("WeakerAccess")
public class ReactionInfo {
    public static final String REACTION_ENTRY_SEPARATOR = " + ";
    public static final String MODIFIER_SEPARATOR = ", ";
    private final SpeciesInformation[] reactants;
    private final SpeciesInformation[] products;
    private final SpeciesInformation[] modifiers;
    private final String reaction;
    private final String id;
    private final String rate;

    private ReactionInfo(SpeciesInformation[] reactants, SpeciesInformation[] products,
            SpeciesInformation[] modifiers, String id, String rate) {
        this.reactants = reactants;
        this.products = products;
        this.modifiers = modifiers;
        this.id = id;
        this.rate = rate;
        this.reaction = constructReactionString(reactants, products, modifiers);
    }

    private ReactionInfo(SpeciesInformation[] lhs, String rhs, String id) {
        if (lhs.length != 1) throw new IllegalArgumentException("The left hand side of a rule should only have one arg");
        this.reactants = lhs;
        this.products = null;
        this.modifiers = null;
        this.reaction = String.format("%s = %s", lhs[0].getSpeciesIdForGlobalAnnotations(), rhs);
        this.id = id;
        this.rate = null;
    }

    private String constructReactionString(SpeciesInformation[] reactants, SpeciesInformation[] products,
            SpeciesInformation[] modifiers) {
        String lhs = joinSpeciesInformationRecords(reactants, REACTION_ENTRY_SEPARATOR);
        String rhs = joinSpeciesInformationRecords(products, REACTION_ENTRY_SEPARATOR);
        String mods = joinSpeciesInformationRecords(modifiers, MODIFIER_SEPARATOR);

        String reactionWithoutModifiers = lhs + " => " + rhs;
        String result;
        if (mods.isEmpty()) {
            result = reactionWithoutModifiers;
        } else {
            result = reactionWithoutModifiers + "; " + mods;
        }

        return result;
    }

    private String joinSpeciesInformationRecords(SpeciesInformation[] reactants, String entrySeparator) {
        return Arrays.stream(reactants)
            .map(SpeciesInformation::getSpeciesIdForGlobalAnnotations)
            .collect(Collectors.joining(entrySeparator));
    }

    public static ReactionInfo parse(Reaction reaction, IExtractionContext extractionContext) {
        String id = reaction.getId();
        String rate = getRate(reaction);

        SpeciesInformation[] reactants = convertToSpeciesInfo(reaction.getListOfReactants(), extractionContext);
        SpeciesInformation[] products  = convertToSpeciesInfo(reaction.getListOfProducts(), extractionContext);
        SpeciesInformation[] modifiers = convertToSpeciesInfo(reaction.getListOfModifiers(), extractionContext);

        return new ReactionInfo(reactants, products, modifiers, id, rate);
    }

    public static ReactionInfo parse(ExplicitRule rule, IExtractionContext extractionContext) {
        String ruleId = Objects.requireNonNull(rule).getId();
        Variable variable = rule.getVariableInstance();
        String id = rule.getVariable();
        if (!(variable instanceof Species)) {
            return null;
        }
        SpeciesInformation[] lhs = extractionContext.getInformationForSpecies(new String[]{id});
        String rhs = rule.getMath().toFormula();
        return new ReactionInfo(lhs, rhs, ruleId);
    }

    private static String getRate(Reaction reaction) {
        if (reaction.getKineticLaw() == null) return "";

        return reaction.getKineticLaw().getMath().toFormula();
    }

    private static SpeciesInformation[] convertToSpeciesInfo(ListOf<? extends SimpleSpeciesReference> references,
            IExtractionContext context) {
        return context.getInformationForSpecies(fetchSpeciesReferences(references));
    }


    private static String[] fetchSpeciesReferences(ListOf<? extends SimpleSpeciesReference> references) {
        return Objects.requireNonNull(references).stream()
            .map(SimpleSpeciesReference::getSpecies)
            .toArray(String[]::new);
    }

    public String getReactantsField() {
        return formatSpeciesLegendSummaries(reactants);
    }

    public String getProductsField() {
        if (null == products) return "";
        return formatSpeciesLegendSummaries(products);
    }

    public String getModifiersField() {
        if (null == modifiers) return "";
        return formatSpeciesLegendSummaries(modifiers);
    }

    public String getId() {
        return id;
    }

    public String getRate() {
        return rate;
    }

    private String formatSpeciesLegendSummaries(SpeciesInformation[] legendSummaries) {
        return Arrays.stream(legendSummaries).sequential()
            .map(SpeciesInformation::asSpeciesLegendSummary)
            .collect(Collectors.joining("£"));
    }

    public String getReaction() {
        return reaction;
    }
}

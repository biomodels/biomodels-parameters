package net.biomodels.parameters.dao;

import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.SelectArg;

import java.sql.SQLException;

/**
 * Created by carankalle on 03/09/2018.
 */
public interface TermResolver<T> {


    public PreparedQuery<T> createLookupStatement(SelectArg selectArg) throws SQLException;
    public T resolve(String accession) throws SQLException;


    }

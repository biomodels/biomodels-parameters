package net.biomodels.parameters.dao;

import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import java.sql.SQLException;

/**
 * Created by carankalle on 09/08/2018.
 * Last Modified by Chinmay Arankalle <carankalle@ebi.ac.uk> on 03/09/2018
 */
@SuppressWarnings("WeakerAccess")
public final class DaoController implements AutoCloseable {
    public static final String DB_URL = "dbUrl";
    public static final String DB_USER = "dbUser";
    public static final String DB_PASSWORD = "dbPassword";
    private final ConnectionSource connectionSource;

    @SuppressWarnings("WeakerAccess")
    public DaoController() throws SQLException {
        this(System.getProperty(DB_URL), System.getProperty(DB_USER), System.getProperty(DB_PASSWORD));
    }

    @SuppressWarnings("WeakerAccess")
    public DaoController(String url, String user, String password) throws SQLException {
        if (null == url || null == user || null == password) {
            throw new IllegalArgumentException("Database url and credentials required");
        }
        this.connectionSource = new JdbcPooledConnectionSource(url, user, password);
        fixOrmLiteLogging();
    }

    @SuppressWarnings("WeakerAccess")
    public ResourceReference getReference(String accession) throws SQLException {
        TermResolver<ResourceReference> defaultTermResolverForResourceReference =
            new DefaultTermResolver<>(connectionSource, ResourceReference.class,
                "accession", "accession", "name", "uri");
        return defaultTermResolverForResourceReference.resolve(accession);
    }

    private void fixOrmLiteLogging() {
        System.setProperty("com.j256.ormlite.logger.type", "LOCAL");
        System.setProperty("com.j256.ormlite.logger.level", "ERROR");
    }

    @SuppressWarnings("WeakerAccess")
    @Override
    public void close() throws Exception {
        if (null != connectionSource) {
            try {
                connectionSource.close();
            } catch (SQLException orig) {
                try {
                    connectionSource.close();
                } catch (SQLException ignore) {
                    // give up and throw the original one
                    throw orig;
                }
            }
        }
    }
}

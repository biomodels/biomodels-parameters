package net.biomodels.parameters.dao;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by carankalle on 09/08/2018.
 */
@DatabaseTable(tableName = "resource_reference")
public class ResourceReference {


    @DatabaseField(columnName = "id", generatedId = true)
    @SuppressWarnings("unused")
    private Integer id;
    @DatabaseField(columnName = "name", dataType = DataType.STRING)
    private String name;
    @DatabaseField(columnName = "accession", dataType = DataType.STRING)
    private String accession;
    @DatabaseField(columnName = "uri", dataType = DataType.STRING)
    private String uri;
    @DatabaseField(columnName = "resolution_status", dataType = DataType.STRING)
    private String resolutionStatus;


    public ResourceReference() {
    }

    public String getResolutionStatus() {
        return resolutionStatus;
    }

    public void setResolutionStatus(String resolutionStatus) {
        this.resolutionStatus = resolutionStatus;
    }

    @SuppressWarnings("WeakerAccess")
    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @SuppressWarnings("unused")
    public String getAccession() {
        return accession;
    }

    @SuppressWarnings("unused")
    public void setAccession(String accession) {
        this.accession = accession;
    }

    public String getFormattedValue() {
        if (null != name && !name.isEmpty() && !name.equalsIgnoreCase("null")) {
            return name;
        } else if (null != accession && !accession.isEmpty() && !accession.equalsIgnoreCase("null")) {
            return accession;
        }
        return "";
    }


    /**
     * This method gets Accession url.
     *
     * @return Combined URL with base uri + accession
     */
    public String getUriWithAccession() {

        if (null != uri && null != accession) {
            return uri + accession;
        }
        return null;

    }
}

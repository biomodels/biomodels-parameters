package net.biomodels.parameters.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by carankalle on 03/09/2018.
 */
public class DefaultTermResolver<T> implements TermResolver  {

    private final Dao<T, Integer> daoObject;
    private final SelectArg selectArg;
    private PreparedQuery<T> accessionLookup;
    private final String[] selectedColumns;
    private final String queryColumn;


    public DefaultTermResolver(Class<T> resolverModel, Dao<T, Integer> daoObject, SelectArg selectArg, PreparedQuery<T> accessionLookup, String queryColumn) {
        this.daoObject = daoObject;
        this.selectArg = selectArg;
        this.accessionLookup = accessionLookup;
        this.queryColumn = queryColumn;
        selectedColumns = new String[0];
    }
    public DefaultTermResolver(ConnectionSource connectionSource, Class<T> resolverModel, String queryColumn, String... selectedColumns) throws SQLException {
        this.queryColumn = queryColumn;
        this.selectedColumns = selectedColumns;
        this.daoObject = DaoManager.createDao(connectionSource, resolverModel);
        this.selectArg = new SelectArg();
        accessionLookup = null;
    }

    public PreparedQuery<T> createLookupStatement(SelectArg selectArg) throws SQLException {
        QueryBuilder<T, Integer> queryBuilder = daoObject.queryBuilder();
        String resolutionStatusColumnName = "resolution_status";
        SelectArg resolutionStatusColumnValue = new SelectArg();
        resolutionStatusColumnValue.setValue("RESOLVED");
        return queryBuilder
            .selectColumns(selectedColumns)
            .where()
            .eq(this.queryColumn, selectArg)
            .and()
            .eq(resolutionStatusColumnName, resolutionStatusColumnValue)
            .prepare();
    }

    public T resolve(String accession) throws SQLException {
        if (null == accession || accession.isEmpty()) {
            return null;
        }

        this.selectArg.setValue(accession);
        this.accessionLookup = this.createLookupStatement(this.selectArg);
        List<T> records = this.daoObject.query(this.accessionLookup);

        if(records.isEmpty()){
            return null;
        }
        return records.get(0);

    }


}

package net.biomodels.parameters;

import java.util.Objects;
import java.util.Optional;

/**
 * @author carankalle on 20/07/18.
 */
@SuppressWarnings("WeakerAccess")
public class ParamsModel implements Cloneable {

    private boolean isCurated = true;
    private String speciesName = "";
    private String speciesMetaID = "";
    private String sboTerm = "";
    private String speciesSboTermUrlString = "";
    private String speciesAnnotation = "";
    private String accessionUrlString = "";
    private String modelID = "";
    private String modelLinks = "";
    private String externalLinks = "";
    private String organism = "";
    private String speciesId = "";
    private String initialData = "";
    private ReactionInfo reactionInfo;

    private String reaction = "";
    private String reactionOriginal = "";
    private String reactionSBOTerm = "";
    private String reactionSBOTermUrlString = "";
    private String products = "";
    private String reactants = "";
    private String modifiers = "";
    private String reactionID = "";
    private String reactionMetaID = "";

    private String equation = "";

    private String equationOriginal = "";

    private String parameters = "";

    private String referencePublication = "";

    private String rateRule = "";
    private String rateRuleOriginal = "";
    private String assignmentRule = "";
    private String assignmentRuleOriginal = "";

    private boolean show = false;

    public boolean getIsCurated() {
        return isCurated;
    }

    public void setIsCurated(boolean curated) {
        isCurated = curated;
    }

    public String getAccessionUrlString() {
        return accessionUrlString;
    }

    public void setAccessionUrlString(String accessionUrlString) {
        this.accessionUrlString = accessionUrlString;
    }

    public String getReactionSBOTermUrlString() {
        return reactionSBOTermUrlString;
    }

    public void setReactionSBOTermUrlString(String reactionSBOTermUrlString) {
        this.reactionSBOTermUrlString = reactionSBOTermUrlString;
    }

    public String getReactionOriginal() {
        return reactionOriginal;
    }

    public void setReactionOriginal(String reactionOriginal) {
        this.reactionOriginal = reactionOriginal;
    }

    public String getSpeciesMetaID() {
        return speciesMetaID;
    }

    public void setSpeciesMetaID(String speciesMetaID) {
        this.speciesMetaID = speciesMetaID;
    }

    public String getSpeciesSboTermUrlString() {
        return speciesSboTermUrlString;
    }

    public void setSpeciesSboTermUrlString(String speciesSboTermUrlString) {
        this.speciesSboTermUrlString = speciesSboTermUrlString;
    }

    public String getInitialData() {
        return initialData;
    }

    public void setInitialData(String initialData) {
        this.initialData = initialData;
    }

    public String getSboTerm() {
        return sboTerm;
    }
    public void setSboTerm(String sboTerm) {
        this.sboTerm = sboTerm;
    }
    public String getSpeciesAnnotation() {
        return speciesAnnotation;
    }

    public void setSpeciesAnnotation(String speciesAnnotation) {
        this.speciesAnnotation = speciesAnnotation;
    }

    public String getSpeciesName() {
        return speciesName;
    }

    public void setSpeciesName(String speciesName) {
        this.speciesName = speciesName;
    }

    public String getReaction() {
        return reaction;
    }

    public void setReaction(String reaction) {
        this.reaction = reaction;
    }

    public String getEquation() {
        return equation;
    }

    public void setEquation(String equation) {
        this.equation = equation;
    }

    public String getEquationOriginal() {
        return equationOriginal;
    }

    public void setEquationOriginal(String equationOriginal) {
        this.equationOriginal = equationOriginal;
    }

    public String getReferencePublication() {
        return referencePublication;
    }

    public void setReferencePublication(String referencePublication) {
        this.referencePublication = referencePublication;
    }

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID;
    }

    public String getModelLinks() {
        return modelLinks;
    }

    public String getExternalLinks() {
        return externalLinks;
    }

    public void setExternalLinks(String externalLinks) {
        this.externalLinks = externalLinks;
    }

    @SuppressWarnings("unused")
    public void setModelLinks(String modelLinks) {
        this.modelLinks = modelLinks;
    }

    public String getReactionID() {
        return reactionID;
    }

    public void setReactionID(String reactionID) {
        this.reactionID = reactionID;
    }

    public String getReactionMetaID() {
        return reactionMetaID;
    }

    @SuppressWarnings("unused")
    public void setReactionMetaID(String reactionMetaID) {
        this.reactionMetaID = reactionMetaID;
    }

    public String getReactionSBOTerm() {
        return reactionSBOTerm;
    }

    public void setReactionSBOTerm(String reactionSBOTerm) {
        this.reactionSBOTerm = reactionSBOTerm;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getReactants() {
        return reactants;
    }

    public void setReactants(String reactants) {
        this.reactants = reactants;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getRateRule() {
        return rateRule;
    }

    public void setRateRule(String rateRule) {
        this.rateRule = rateRule;
    }

    public String getRateRuleOriginal() {
        return rateRuleOriginal;
    }

    public void setRateRuleOriginal(String rateRuleOriginal) {
        this.rateRuleOriginal = rateRuleOriginal;
    }

    public String getAssignmentRule() {
        return assignmentRule;
    }

    public void setAssignmentRule(String assignmentRule) {
        this.assignmentRule = assignmentRule;
    }

    public String getAssignmentRuleOriginal() {
        return assignmentRuleOriginal;
    }

    public void setAssignmentRuleOriginal(String assignmentRuleOriginal) {
        this.assignmentRuleOriginal = assignmentRuleOriginal;
    }

    public boolean getShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public String getOrganism() {
        return organism;
    }

    public void setOrganism(String organism) {
        this.organism = organism;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "\r\nModel ID: " + this.getModelID() +
            "\r\nModel Links: " + this.getModelLinks() +
            "\r\nSpecies Name: " + this.getSpeciesName() +
            "\r\nSpecies Annotation: " + this.getSpeciesAnnotation() +
            "\r\nSpecies Meta ID: " + this.getSpeciesMetaID() +
            "\r\nSpecies SBO Term: " + this.getSboTerm() +
            "\r\nSpecies Initial Data : " + this.getInitialData() +
            "\r\nReaction ID : " + this.getReactionID() +
            "\r\nReaction SBOTerm : " + this.getReactionSBOTerm() +
            "\r\nReaction Meta ID : " + this.getReactionMetaID() +
            "\r\nReactants : " + this.getReactants() +
            "\r\nProducts : " + this.getProducts() +
            "\r\nReaction : " + this.getReaction() +
            "\r\nEquation : " + this.getEquation() +
            "\r\nRate Rule : " + this.getRateRule() +
            "\r\nAssignment Rule : " + this.getAssignmentRule() +
            "\r\nParameters : " + this.getParameters() +
            "\r\nReference Publication : " + this.getReferencePublication() +
            "\r\nShow record : " + this.getShow() +

            "\r\n|--------------------------------------------------------------------|";
    }

    public String getSpeciesId() {
        return speciesId;
    }

    public void setSpeciesId(String speciesId) {
        this.speciesId = speciesId;
    }

    public void setReactionInfo(ReactionInfo reactionInfo) {
        this.reactionInfo = Objects.requireNonNull(reactionInfo);
        reaction = reactionInfo.getReaction();
        reactionOriginal = reactionInfo.getReaction();
        reactants = reactionInfo.getReactantsField();
        products = reactionInfo.getProductsField();
        modifiers = reactionInfo.getModifiersField();
        equation = reactionInfo.getRate();
        equationOriginal = reactionInfo.getRate();
    }

    public ReactionInfo getReactionInfo() {
        return reactionInfo;
    }

    public String getModifiers() {
        return modifiers;
    }
}

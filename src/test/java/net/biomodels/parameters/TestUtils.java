package net.biomodels.parameters;

import net.biomodels.parameters.dao.DaoController;
import org.h2.jdbcx.JdbcConnectionPool;
import org.h2.tools.RunScript;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.SBMLDocument;
import org.sbml.jsbml.SBMLReader;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.SQLException;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

/**
 * Created by carankalle on 19/09/2018.
 */
@SuppressWarnings("WeakerAccess")
public final class TestUtils {

    public static final String RESOURCE_PATH = "./src/test/resources";
    private static final String DB_URL = "jdbc:h2:mem:test;MVCC=true;DB_CLOSE_ON_EXIT=FALSE";
    private static final String DB_USER = "sa";
    private static final String DB_PASSWORD = "sa";

    public void setExtractionService(ExtractionService extractionService) {
        this.extractionService = extractionService;
    }

    private ExtractionService extractionService;

    public JdbcConnectionPool createConnectionAndImportData() throws SQLException, FileNotFoundException {
        JdbcConnectionPool cp = JdbcConnectionPool.create(DB_URL, DB_USER, DB_PASSWORD);
        RunScript.execute(cp.getConnection(), new FileReader(RESOURCE_PATH + "/resource_reference.sql"));
        return cp;
    }

    public Model getModelFromLocal(String filename) {
        File file = new File(RESOURCE_PATH, filename);
        try {
            return extractionService.getModel(file);
        } catch (XMLStreamException ex) {
            fail("Unable to load the Model " + ex.getMessage());
        } catch (IOException e) {
            fail("IOException: Unable to locate the model file " + e.getMessage());
        }
        throw new IllegalStateException("this should never happen");
    }

    public DaoController initializeDaoController() throws SQLException {
        return new DaoController(DB_URL, DB_USER, DB_PASSWORD);
    }

    public static Model parseTestModel(String fileName) throws IOException, XMLStreamException {
        final File model = new File(RESOURCE_PATH, fileName);
        SBMLDocument doc = new SBMLReader().readSBML(model);
        assertNotNull("Could not parse model file " + model, doc);
        return doc.getModel();
    }

}

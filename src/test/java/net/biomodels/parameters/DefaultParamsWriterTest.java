package net.biomodels.parameters;

import net.biomodels.parameters.dao.DaoController;
import net.biomodels.parameters.exceptions.CustomException;
import net.biomodels.parameters.exceptions.SpeciesNotFoundException;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.sbml.jsbml.Model;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class DefaultParamsWriterTest {

    private final String configPath = "./src/test/resources/config.properties";
    private TestUtils testUtils = new TestUtils();
    private String resourcePATH = TestUtils.RESOURCE_PATH;
    private ExtractParam extractParam = null;
    private JdbcConnectionPool cp = null;
    private ArgumentParser argumentParser = null;
    private ExtractionService extractionService;

    @Before
    public void setUp() throws SQLException, FileNotFoundException {
        cp = testUtils.createConnectionAndImportData();
        DaoController daoController = testUtils.initializeDaoController();
        RequestContext requestContext = new RequestContext(daoController);
        extractionService = new ExtractionService(requestContext);
        extractParam = new ExtractParam(requestContext);
        testUtils.setExtractionService(extractionService);
        argumentParser = new ArgumentParser();
    }

    @After
    public void tearDown() {
        if (null != cp) {
            cp.dispose();
        }
        if (null != extractParam) {
            extractParam.getRequestContext().close();
        }
    }

    @Test
    public void testNullRecordPreformat() throws IOException, XMLStreamException, SpeciesNotFoundException, CustomException {
        String filename = "BIOMD0000000005.xml";
        File file = new File(resourcePATH, filename);
        List<ParamsModel> paramsModels = extractionService.extractAllParameters(file);
        File csvFile = new File(resourcePATH, filename + ".csv");
        csvFile.deleteOnExit();
        extractParam.getDefaultParamsWriter().writeToCSV(paramsModels, csvFile);
        Stream<String> csvLines = Files.lines(csvFile.toPath(), StandardCharsets.UTF_8);
        Optional<String> cyclinRow = csvLines.filter(row ->
            row.contains("null")
        ).findAny();
        assertFalse(cyclinRow.isPresent());
    }

    @Test
    public void testWriteToCSV() throws IOException, XMLStreamException, SpeciesNotFoundException, CustomException {
            String filename = "BIOMD0000000006.xml";
            Model jsbmlModel = testUtils.getModelFromLocal(filename);
            File file = new File(resourcePATH, filename);
            List<ParamsModel> paramsModels = extractionService.extractAllParameters(file);

            assertFalse(paramsModels != null && paramsModels.isEmpty());
            assertFalse(paramsModels.isEmpty());
            assertEquals(jsbmlModel.getNumSpecies(), extractionService.getExtractionContext().getGlobalSpeciesAnnotations().size());

            File csvFile = new File(resourcePATH, filename + ".csv");
            csvFile.deleteOnExit();
            extractParam.getDefaultParamsWriter().writeToCSV(paramsModels, csvFile);
            assertTrue(csvFile.length() > 0);

            String speciesAnnotation =
                "[cyclin-dependent protein serine/threonine kinase activity, MPF complex]";
            String startMarker = '"' + speciesAnnotation;
            String reaction = "\"([cyclin-dependent protein serine/threonine kinase activity, MPF complex]) => (EmptySet)\"";
            String reactionSboLink = "\"https://identifiers.org/sbo/SBO:0000205|SBO:0000205\"";
            String entitySboLink = "\"https://identifiers.org/sbo/SBO:0000297|protein complex\"";
            String accesionUrl =
                "\"http://identifiers.org/go/GO:0004693|cyclin-dependent protein serine/threonine kinase activity, http://identifiers.org/go/GO:0031387|MPF complex\"";
            String parameters = "\"k6 = 1.0\"";
            Stream<String> csvLines = Files.lines(csvFile.toPath(), StandardCharsets.UTF_8);
            Optional<String> cyclinRow = csvLines.filter(row ->
                (row.startsWith(startMarker)
                    && row.contains(reaction)
                    && row.contains(parameters)
                    && row.contains("protein complex")
                    && !row.contains("IPR")
                    && row.contains(accesionUrl)
                )
                    || row.contains(reactionSboLink)
                    && row.contains(entitySboLink)
            ).findAny();
            assertTrue(cyclinRow.isPresent());
    }
}

package net.biomodels.parameters;

import net.biomodels.parameters.dao.DaoController;
import net.biomodels.parameters.dao.ResourceReference;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.IOException;
import java.sql.SQLException;

import static org.junit.Assert.*;

/**
 * @author Mihai Glont <mihai.glont@ebi.ac.uk> on 14/08/2018
 */
@RunWith(JUnit4.class)
public class DaoControllerTest {
    private JdbcConnectionPool cp = null;
    private DaoController controller = null;
    private final String url = ExtractParamTest.DB_URL;
    private final String user = ExtractParamTest.DB_USER;
    private final String password = ExtractParamTest.DB_PASSWORD;

    @Before
    public void setUp() throws SQLException, IOException {
        TestUtils testUtils = new TestUtils();
        cp = testUtils.createConnectionAndImportData();
        controller = testUtils.initializeDaoController();
    }

    @Test()
    public void testSystemPropertyDaoControllerInstantiation() {
        setSystemProperties(url, user, password);

        try {
            controller = new DaoController();
            final ResourceReference result = controller.getReference("PATO:0001462");
            assertEquals("membrane potential", result.getName());
        } catch (SQLException se) {
            fail("SQL Exception occurred, this test failed " + se.getMessage());
        }

        try {
            controller = new DaoController(null, user, password);
            fail("IllegalArgumentException expected when using a null url");
        } catch (IllegalArgumentException expected) {
            assertEquals("Database url and credentials required", expected.getMessage());
        } catch (SQLException se) {
            fail("SQL Exception occurred, this test failed " + se.getMessage());
        }
    }

    @Test
    public void testResolveSpecies() {
        try {
            ResourceReference resourceReference = controller.getReference("P04551");
            assertEquals("Cyclin-dependent kinase 1", resourceReference.getName());
        } catch (SQLException se) {
            fail("SQL Exception occurred, this test failed " + se.getMessage());

        }


    }

    @Test
    public void testNullResolveSpecies() {
        ResourceReference resourceReference = null;
        try {
            resourceReference = controller.getReference("");
            assertNull(resourceReference);
        } catch (SQLException e) {
            fail("SQL Exception occurred, this test failed " + e.getMessage());
        }
    }

    @Test
    public void testSqlInjectionWhenResolvingSpecies() {
        try {
            ResourceReference resourceReference = controller.getReference("P12345' OR '1' = '1");
            assertNull(resourceReference);
        } catch (SQLException se) {
            fail("SQL Exception occurred, this test failed " + se.getMessage());
        }

    }

    @After
    public void tearDown() {
        if (null != cp) {
            cp.dispose();
        }
        if (null != controller) {
            try {
                controller.close();
            } catch (Exception e) {
                System.err.println("Error while closing controller: " + e);
            }
        }
    }

    private void setSystemProperties(String url, String user, String password) {
        System.setProperty(DaoController.DB_URL, url);
        System.setProperty(DaoController.DB_USER, user);
        System.setProperty(DaoController.DB_PASSWORD, password);
    }
}

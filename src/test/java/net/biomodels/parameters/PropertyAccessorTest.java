package net.biomodels.parameters;

import net.biomodels.parameters.exceptions.PropertiesNotFound;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

@RunWith(JUnit4.class)
public class PropertyAccessorTest {

    private PropertyAccessor propertyAccessor;
    private final String configPath = "./src/test/resources/config.properties";

    @Before
    public void setUp() {
        try {
            propertyAccessor = new PropertyAccessor(configPath);
        } catch (PropertiesNotFound propertiesNotFound) {
            fail(propertiesNotFound.getMessage());
        }
    }

    @Test
    public void testGeBlackListedProperty() {
        try {
            String actualValue = propertyAccessor.getProperty("blacklisted_species");
            String expectedValue = "EmptySet";
            assertEquals(expectedValue, actualValue);
        } catch (PropertiesNotFound propertiesNotFound) {
            fail(propertiesNotFound.getMessage());
        }
    }

    @Test(expected = PropertiesNotFound.class)
    public void testGeBlackListedPropertyWithExceptin() throws PropertiesNotFound {
        propertyAccessor.getProperty("WRONG_KEY");
    }
}

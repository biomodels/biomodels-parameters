package net.biomodels.parameters;

import org.junit.Test;
import org.sbml.jsbml.ListOf;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Reaction;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ReactionInfoTest {

    @Test
    public void parse() throws IOException, XMLStreamException {
        Model model = TestUtils.parseTestModel("BIOMD0000000001.xml");
        assertNotNull("BIOMD0000000001.xml was not parsed properly by JSBML", model);
        ListOf<Reaction> reactions = model.getListOfReactions();
        assertEquals(17, reactions.size());

        Reaction r0 = reactions.get(0);
        ReactionInfo info = ReactionInfo.parse(r0, new ExtractionContext());
        assertEquals("React0", info.getId());
        assertEquals("comp1*(kf_0*B-kr_0*BL)", info.getRate());

        assertEquals("", info.getReactantsField());
        assertEquals("", info.getProductsField());
        assertEquals("", info.getModifiersField());
    }
}

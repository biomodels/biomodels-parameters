package net.biomodels.parameters;

import net.biomodels.parameters.dao.DaoController;
import net.biomodels.parameters.exceptions.CustomException;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author carankalle on 21/11/2018.
 */
public class ArgumentParserTest {
    private ArgumentParser argumentParser;
    private RequestContext requestContext;
    private JdbcConnectionPool cp;

    @Before
    public void setUp() throws Exception {
        TestUtils testUtils = new TestUtils();
        cp = testUtils.createConnectionAndImportData();
        DaoController daoController = testUtils.initializeDaoController();
        requestContext = new RequestContext(daoController);
        argumentParser = new ArgumentParser();
    }

    @After
    public void tearDown() {
        if (null != cp) {
            cp.dispose();
        }
        if (null != argumentParser) {
            requestContext.close();
        }
    }

    @Test
    public void testParseCliArgumentsPositively() {
        String[] arguments = {"TEST_MODEL_ID,TEST_BIOMD_ID, TEST_INPUT_PATH", "TEST_OUTPUT_PATH"};
        try {
            requestContext = argumentParser.parseCliArgument(arguments, requestContext);
        } catch (Exception ex) {
            fail(ex.getMessage());
        }
        assertEquals(arguments[0].split(",")[1], requestContext.getBiomdIdFromRequestContext());
        assertEquals(arguments[0].split(",")[0], requestContext.getModelIdFromRequestContext());
        assertEquals(arguments[0].split(",")[2], requestContext.getSourceFileName());
        assertEquals(arguments[1], requestContext.getDestinationFilePath());
    }

    @Test(expected = CustomException.class)
    public void testParseCliArgumentsNegatively() throws CustomException {
        String[] arguments = {};
        argumentParser.parseCliArgument(arguments, requestContext);
    }

    @Test(expected = CustomException.class)
    public void testParseCliArgumentsWWithNoIdentifiers() throws CustomException {
        String[] arguments = {"TEST_INPUT_PATH", "TEST_OUTPUT_PATH", "TEST_PROPERTIES_PATH", "all"};
        argumentParser.parseCliArgument(arguments, requestContext);
    }
}

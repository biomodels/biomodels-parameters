package net.biomodels.parameters;

import net.biomodels.parameters.dao.DaoController;
import net.biomodels.parameters.exceptions.CustomException;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import static org.junit.Assert.*;

/**
 * @author carankalle on 01/08/2018.
 */
@SuppressWarnings("SameParameterValue")
public class ExtractParamTest {
    @SuppressWarnings("WeakerAccess")
    public static final String DB_URL = "jdbc:h2:mem:test";
    @SuppressWarnings("WeakerAccess")
    public static final String DB_USER = "sa";
    @SuppressWarnings("WeakerAccess")
    public static final String DB_PASSWORD = "sa";
    private final TestUtils testUtils = new TestUtils();
    private String resourcePATH = TestUtils.RESOURCE_PATH;
    private final String configPath = resourcePATH + "/config.properties";
    private JdbcConnectionPool cp = null;
    private ExtractParam extractParam = null;
    private Map<String, String> filenamePrefixModelIdMap = new HashMap<>();

    private Stream<String> runIntegration(String modelId, String biomdId, String filename) {
        String[] args = {modelId + "," + biomdId + "," + resourcePATH + filename, resourcePATH, configPath};
        return runModuleForTest(filename, args);
    }

    private Stream<String> runIntegration(String modelId, String filename) {
        String[] args = {modelId + "," + "" + "," + resourcePATH + filename, resourcePATH, configPath};
        return runModuleForTest(filename, args);
    }

    private void executeTestAndCheckTestString(String modelId, String biomdId, String fileName, String testString) {
        Stream<String> csvLines = this.runIntegration(modelId, biomdId, fileName);
        if (csvLines == null) {
            fail("No outputfile found");
        }
        checkTestString(testString, csvLines);
    }

    private void executeTestAndCheckTestString(String modelId, String fileName, String testString) {
        Stream<String> csvLines = this.runIntegration(modelId, fileName);
        if (csvLines == null) {
            fail("No outputfile found");
        }
        checkTestString(testString, csvLines);
    }

    private void checkTestString(String testString, Stream<String> csvLines) {

        Optional<String> cyclinRow = csvLines.filter(row ->
            row.contains(testString))
            .findAny();
        assertTrue(cyclinRow.isPresent());
    }

    private Stream<String> runModuleForTest(String filename, String[] args) {
        try {
            extractParam.runModule(args);
            File csvFile = new File(resourcePATH, filename + ".csv");
            File jsonFile = new File(resourcePATH, filename + ".csv.json");
            csvFile.deleteOnExit();
            jsonFile.deleteOnExit();
            if (!csvFile.exists()) {
                fail("No outout found of runModule method");
            }

            return Files.lines(csvFile.toPath(), StandardCharsets.UTF_8);
        } catch (CustomException | IOException e) {
            fail(e.getMessage());
        }
        return null;
    }

    private String getModelId(String filenamePrefix) {
        return this.filenamePrefixModelIdMap.get(filenamePrefix);
    }

    @Before
    public void setUp() throws SQLException, FileNotFoundException {

        cp = testUtils.createConnectionAndImportData();
        DaoController daoController = testUtils.initializeDaoController();
        RequestContext requestContext = new RequestContext(daoController);
        extractParam = new ExtractParam(requestContext);
        filenamePrefixModelIdMap.put("BIOMD0000000001", "MODEL6615048798");
        filenamePrefixModelIdMap.put("BIOMD0000000009", "MODEL6615048798");
        filenamePrefixModelIdMap.put("BIOMD0000000460", "MODEL1302080000");
        filenamePrefixModelIdMap.put("BIOMD0000000006", "MODEL6614715255");
        filenamePrefixModelIdMap.put("BIOMD0000000024", "MODEL6618241436");
        filenamePrefixModelIdMap.put("BIOMD0000000015", "MODEL6617035399");
        filenamePrefixModelIdMap.put("BIOMD0000000724", "MODEL1812040008");
        filenamePrefixModelIdMap.put("BIOMD0000000647", "MODEL1708250000");
        filenamePrefixModelIdMap.put("BIOMD0000000679", "MODEL1006230002");
        filenamePrefixModelIdMap.put("BIOMD0000000493", "MODEL1310150000");
        filenamePrefixModelIdMap.put("BIOMD0000000596", "MODEL1508180000");
        filenamePrefixModelIdMap.put("BIOMD0000000005", "MODEL6614644188");
        filenamePrefixModelIdMap.put("BIOMD0000000053", "MODEL0733584307");
        filenamePrefixModelIdMap.put("MA-p52-IkBd-Fig3B", "MODEL1904030001");

    }

    @After
    public void tearDown() {
        if (null != cp) {
            cp.dispose();
        }
        if (null != extractParam) {
            extractParam.getRequestContext().close();
        }
    }

    @Test
    public void testRunModuleWithReactionSbo() {

        String publicationId = "BIOMD0000000001";
        String modelId = getModelId(publicationId);
        Stream<String> csvLines = this.runIntegration(modelId, publicationId, "/" + publicationId + ".xml");
        if (csvLines == null) {
            fail("No outputfile found");
        }
        Optional<String> cyclinRow = csvLines.filter(row ->
            row.contains("non-covalent binding"))
            .findAny();
        assertTrue(cyclinRow.isPresent());
    }

    @Test
    public void testRunModuleWithOrganism() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000460";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000460.xml", "Bacillus subtilis");
    }

    @Test
    public void testRunModuleAndTestPublicationValueWithSpecialChars() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000647";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId,filenamePrefixOrBiomdId,"/BIOMD0000000647_url.xml", "http://identifiers.org/mamo/MAMO_0000046");

    }

    @Test
    public void testRunModuleWithModelWithoutUnitDefinition() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000493";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000493_url.xml", "BIOMD0000000493");
    }

    @Test
    public void testRunModuleWithModelWithoutKineticLaw() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000596";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000596_url.xml", "BIOMD0000000596");
    }

    @Test
    public void testRunModuleWithoutResolvedOrganism() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000006";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000006_modified.xml", "331544");
    }

    @Test
    public void testRunModuleWithMultipleOrganisms() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000024";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000024.xml", "Mesocricetus auratus; Drosophila melanogaster");
    }

    @Test
    public void testRunModuleAndPreFilterIntegrationForUnits() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000015";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000015.xml", "5.01742 μmol");
    }

    @Test
    public void testRunModuleAndWithGlobalParametersUnits() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000724";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/Theinmozhi_2018.xml", "l/(nmol*s)");
    }

    @Test
    public void testRunModuleAndWithGlobalParametersUnits2() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000679";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000679_urn.xml", "200.0 mol");
    }

    @Test
    public void testRunModuleAndWithCuratedModel() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000679";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000679_urn.xml", "true");
    }

    @Test
    public void testRunModuleAndWithNonCuratedModel() {
        String modelId = getModelId("MA-p52-IkBd-Fig3B");
        executeTestAndCheckTestString(modelId, "/MA-p52-IkBd-Fig3B.xml", "false");
    }

    @Test
    public void testRunModuleAndFilterNullValueCheck() {

        final String filenamePrefixOrBiomdId = "BIOMD0000000005";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        Stream<String> csvLines = this.runIntegration(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000005.xml");
        if (csvLines == null) {
            fail("No outputfile found");
        }
        Optional<String> cyclinRow = csvLines.filter(row ->
            row.contains("null"))
            .findAny();
        assertFalse(cyclinRow.isPresent());
    }

    @Test
    public void testRunModuleReactionOriginalCheck() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000005";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000005.xml", "M => pM");
    }

    @Test
    public void testRunModuleReactomeExternalLinks() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000005";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000005.xml", "sabiork.compound:P04551; reactome:P04551");
    }

    @Test
    public void testRunModuleSabioRKExternalLinks() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000053";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000053_url.xml",
            "sabiork.compound:ChebiID:17234; sabiork.compound:C00293");
    }


    @Test
    public void testRunModuleRateOriginalCheck() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000006";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000006_modified.xml", "k4*z*(k4prime/k4+u^2)");
    }


    @Test
    public void testRunModuleAssignmentOriginalCheck() {
        final String filenamePrefixOrBiomdId = "BIOMD0000000679";
        String modelId = getModelId(filenamePrefixOrBiomdId);
        executeTestAndCheckTestString(modelId, filenamePrefixOrBiomdId, "/BIOMD0000000679_urn.xml", "tau1*T^3+tau2*T^2+tau3*T+tau4");
    }

    @Test
    public void getParamsFromModelFile() throws IOException {
        final File dotGitIgnore = new File(".gitignore");
        assertTrue(dotGitIgnore.isFile());
        File targetFolder = new File("target");
        assertTrue(targetFolder.isDirectory());
        extractParam.extract(dotGitIgnore, targetFolder.getAbsolutePath());
        String[] matches = targetFolder.list((dir, name) -> dotGitIgnore.getName().equals(name));
        assertNotNull(matches);
        assertEquals(0, matches.length);
    }

    @Test(expected = IOException.class)
    public void getParamsFromModelFileWithBrokenOutputFolder() throws IOException {
        File nowhere = new File("nowhere");
        assertFalse(nowhere.exists());
        extractParam.extract(new File(TestUtils.RESOURCE_PATH,"BIOMD0000000001.xml"), null);
        Assert.fail("IOException expected but not thrown");
    }
}

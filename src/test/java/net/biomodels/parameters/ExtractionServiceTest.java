package net.biomodels.parameters;

import net.biomodels.parameters.dao.DaoController;
import net.biomodels.parameters.exceptions.CustomException;
import net.biomodels.parameters.exceptions.SpeciesNotFoundException;
import org.h2.jdbcx.JdbcConnectionPool;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Species;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.*;

/**
 * @author carankalle on 23/11/2018.
 */
public class ExtractionServiceTest {
    private final TestUtils testUtils = new TestUtils();
    private RequestContext requestContext;
    private JdbcConnectionPool cp = null;
    private ExtractionService extractionService;

    private ParamsModel extractOrganismFromLocalModel(String filename) {
        Model jsbmlModel = testUtils.getModelFromLocal(filename);
        ParamsModel paramsModel = new ParamsModel();
        paramsModel = extractionService.extractOrganism(paramsModel, jsbmlModel);
        return paramsModel;
    }

    private Optional<ParamsModel> getOptionalParamsModelWithSpeciesId(List<ParamsModel> paramsModels, String speciesId) {
        return paramsModels
            .stream()
            .filter(paramsModel ->
                paramsModel.getSpeciesId().equals(speciesId))
            .findAny();
    }

     private Map<String, SpeciesInformation> getAnnotationsFromLocal(@SuppressWarnings("SameParameterValue") String model) {
        Model jsbmlModel = testUtils.getModelFromLocal(model);
        extractionService.initializeExtractionContext(jsbmlModel);
        return extractionService.getAnnotationsFromSpecies(jsbmlModel, extractionService.getExtractionContext());
    }
    @Before
    public void setUp() throws Exception {
        cp = testUtils.createConnectionAndImportData();
        DaoController daoController = testUtils.initializeDaoController();
        requestContext = new RequestContext(daoController);
        extractionService = new ExtractionService(requestContext);
        testUtils.setExtractionService(extractionService);
    }

    @After
    public void tearDown() {
        if (null != cp) {
            cp.dispose();
        }
        if (null != requestContext) {
            requestContext.close();
        }
    }

    private List<ParamsModel> getData(String filename) throws SpeciesNotFoundException {
        File file = new File(TestUtils.RESOURCE_PATH, filename);
        try {
            return extractionService.extractAllParameters(file);
        } catch (XMLStreamException mexc) {
            fail("Unable to load the Model " + mexc.getMessage());
        } catch (IOException | CustomException e) {
            fail("Unable extract parameters due to : " + e.getMessage());
        }
        return null;
    }

    @Test
    public void testGetModelPositive() {
        assertTrue(testUtils.getModelFromLocal("BIOMD0000000006.xml").isSetId());

    }

    @Test(expected = IOException.class)
    public void testGetModelNegative() throws IOException, XMLStreamException {
        File file = new File("NOT_CORRECT_PATH");
        extractionService.getModel(file);
    }

    @Test(expected = SpeciesNotFoundException.class)
    public void testGetSpeciesExtractionNegative() throws SpeciesNotFoundException {
        this.getData("/BIOMD0000000238.xml");
    }

    @Test
    public void testGetSpeciesAnnotationPositive() {
        Map<String, SpeciesInformation> speciesAnnotations = getAnnotationsFromLocal("BIOMD0000000378.xml");
        String expected = "[gated channel activity]";
        String actual = speciesAnnotations.get("d").getSpeciesAnnotationString();
        assertEquals(expected, actual);
    }

    @Test
    public void testReactionInfo() {
        try {
            List<ParamsModel> paramsModels = this.getData("BIOMD0000000005.xml");
            assertNotNull(paramsModels);

            IExtractionContext ctx = extractionService.getExtractionContext();
            assertEquals(9, ctx.getReactionInfoMap().size());
            ReactionInfo reaction9 = ctx.getReactionInfo("Reaction9");
            assertNotNull(reaction9);
            ParamsModel pMReaction9 = paramsModels.stream()
                .filter(m -> m.getSpeciesId().equals("pM") && m.getReactionID().equals("Reaction9"))
                .findAny()
                .orElseThrow(() -> new AssertionError("Missing ParamsModel entry for Reaction9 and species pM"));

            assertEquals("pM => M; CT", pMReaction9.getReaction());
            assertEquals("cell*pM*(k4prime+k4*(M/CT)^2)", pMReaction9.getEquation());

            assertEquals("pM$p-cyclin_cdc2-p$http://identifiers.org/uniprot/P04551|Cyclin-dependent kinase 1; IPR006670", reaction9.getReactantsField());
            assertEquals("M$p-cyclin_cdc2$http://identifiers.org/uniprot/P04551|Cyclin-dependent kinase 1; IPR006670", reaction9.getProductsField());
            assertEquals("CT$total_cdc2$http://identifiers.org/uniprot/P04551|Cyclin-dependent kinase 1", reaction9.getModifiersField());

            Optional<ParamsModel> optionalParamsModel = getOptionalParamsModelWithSpeciesId(paramsModels, "C2");
            assertTrue(optionalParamsModel.isPresent());
            String expectedReaction = "M => C2 + YP";
            String actualReactionOriginal = optionalParamsModel.get().getReactionOriginal();
            String actualReaction = optionalParamsModel.get().getReaction();

            assertEquals(expectedReaction, actualReactionOriginal);
            assertEquals(expectedReaction, actualReaction);

        } catch (SpeciesNotFoundException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testEquationOrRateOriginalInfo() {
        try {
            List<ParamsModel> paramsModels = this.getData("BIOMD0000000006_modified.xml");
            assertNotNull(paramsModels);
            Optional<ParamsModel> optionalParamsModel = getOptionalParamsModelWithSpeciesId(paramsModels, "u");
            assertTrue(optionalParamsModel.isPresent());
            String expectedEquationOriginal = "k6*u";
            String actualRateOriginal = optionalParamsModel.get().getEquationOriginal();
            assertEquals(expectedEquationOriginal, actualRateOriginal);

        } catch (SpeciesNotFoundException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testAssignmentOriginalInfo() {
        try {
            List<ParamsModel> paramsModels = this.getData("BIOMD0000000679_urn.xml");
            assertNotNull(paramsModels);
            Optional<ParamsModel> optionalParamsModel = getOptionalParamsModelWithSpeciesId(paramsModels, "K_T");
            assertTrue(optionalParamsModel.isPresent());
            String expectedAssignmentOriginal = "tau1*T^3+tau2*T^2+tau3*T+tau4";
            String actualAssignmentOriginal = optionalParamsModel.get().getAssignmentRuleOriginal();
            assertEquals(expectedAssignmentOriginal, actualAssignmentOriginal);

        } catch (SpeciesNotFoundException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testModelWithoutReaction() {
        try {
            List<ParamsModel> paramsModels = this.getData("BIOMD0000000378.xml");
            assertNotNull(paramsModels);
            Optional<ParamsModel> optionalParamsModel = getOptionalParamsModelWithSpeciesId(paramsModels, "V_membrane");
            assertTrue(optionalParamsModel.isPresent());
            // FIXME rule info should not go into ReactionInfo
            //assertTrue(optionalParamsModel.get().getReactionOriginal().isEmpty());
            //assertTrue(optionalParamsModel.get().getReaction().isEmpty());

        } catch (SpeciesNotFoundException e) {
            fail(e.toString());
        }
    }

    @Test
    public void testGetSpeciesAnnotationNegative() {
        Model jsbmlModel = testUtils.getModelFromLocal("BIOMD0000000378.xml");
        ExtractionService extractionService = new ExtractionService(new RequestContext(null));
        extractionService.initializeExtractionContext(jsbmlModel);
        Map<String, SpeciesInformation> speciesAnnotations =
            extractionService.getAnnotationsFromSpecies(jsbmlModel, extractionService.getExtractionContext());
        String expected = "[GO:0022836]";
        String actual = speciesAnnotations.get("d").getSpeciesAnnotationString();
        assertEquals(expected, actual);
    }

    @Test
    public void testUnresolvedReactantProductModifierInfo() {
        Model jsbmlModel = testUtils.getModelFromLocal("BIOMD0000000005.xml");
        ExtractionService extractionService = new ExtractionService(new RequestContext(null));
        extractionService.initializeExtractionContext(jsbmlModel);
        ReactionInfo reactionInfo = extractionService.getExtractionContext().getReactionInfo("Reaction9");
        assertNotNull(reactionInfo);
        assertEquals("pM$p-cyclin_cdc2-p$P04551; IPR006670", reactionInfo.getReactantsField());
        assertEquals("M$p-cyclin_cdc2$P04551; IPR006670", reactionInfo.getProductsField());
        assertEquals("CT$total_cdc2$P04551", reactionInfo.getModifiersField());
    }

    @Test
    public void testResolvedReactantInfo() throws SQLException {
        DaoController daoController = testUtils.initializeDaoController();
        Model jsbmlModel = testUtils.getModelFromLocal("BIOMD0000000005.xml");
        ExtractionService extractionService = new ExtractionService(new RequestContext(daoController));
        extractionService.initializeExtractionContext(jsbmlModel);
        ReactionInfo info = extractionService.getExtractionContext().getReactionInfo("Reaction9");
        assertNotNull(info);
        assertEquals("CT$total_cdc2$http://identifiers.org/uniprot/P04551|Cyclin-dependent kinase 1", info.getModifiersField());
    }

    @Test
    public void testSabioRKExternalLinks() {
        Model jsbmlModel = testUtils.getModelFromLocal("BIOMD0000000053_url.xml");
        ExtractionService extractionService = new ExtractionService(new RequestContext(null));
        extractionService.initializeExtractionContext(jsbmlModel);
        Map<String, SpeciesInformation> speciesAnnotations =
            extractionService.getAnnotationsFromSpecies(jsbmlModel, extractionService.getExtractionContext());
        String expectedExternalLinkString = "sabiork.compound:ChebiID:17234; sabiork.compound:C00293";
        final SpeciesInformation speciesInformation = speciesAnnotations.get("Glucose");
        String actualExternalLinkString = speciesInformation.getAnnotationExternalLinkString();
        assertEquals(expectedExternalLinkString, actualExternalLinkString);

        for (String accessionWithSuffix : actualExternalLinkString.split(";")) {
            String[] accessionWithSuffixArr = accessionWithSuffix.split(":");
            final String accession = accessionWithSuffixArr[accessionWithSuffixArr.length - 1];
            final String prefix = accessionWithSuffixArr[0];
            if (null != prefix && prefix.equals("sabiork.compound")) {
                assertTrue(extractionService.getExtractionContext().getSabioRKSet().contains(accession));
            }
        }
    }

    @Test
    public void testReactomeExternalLinks() {
        Model jsbmlModel = testUtils.getModelFromLocal("BIOMD0000000005.xml");
        ExtractionService extractionService = new ExtractionService(new RequestContext(null));
        extractionService.initializeExtractionContext(jsbmlModel);
        Map<String, SpeciesInformation> speciesAnnotations =
            extractionService.getAnnotationsFromSpecies(jsbmlModel, extractionService.getExtractionContext());
        String expectedExternalLinkString = "sabiork.compound:P04551; reactome:P04551";
        final SpeciesInformation speciesInformation = speciesAnnotations.get("CP");
        String actualExternalLinkString = speciesInformation.getAnnotationExternalLinkString();
        assertEquals(expectedExternalLinkString, actualExternalLinkString);
    }

    @Test
    public void testExtractOrganismWithResolvableOrganismValue() {

        ParamsModel paramsModel = extractOrganismFromLocalModel("BIOMD0000000460.xml");
        String expectedOrganism = "Bacillus subtilis (strain 168)";
        String actualOrganism = paramsModel.getOrganism();
        assertEquals(expectedOrganism, actualOrganism);

    }

    @Test
    public void testExtractOrganismWithMultipleOrganismValue() {

        ParamsModel paramsModel = extractOrganismFromLocalModel("BIOMD0000000024.xml");
        String expectedOrganism = "Mesocricetus auratus; Drosophila melanogaster";
        String actualOrganism = paramsModel.getOrganism();
        assertEquals(expectedOrganism, actualOrganism);

    }

    @Test
    public void testExtractOrganismWithoutOrganismValue() {

        ParamsModel paramsModel = extractOrganismFromLocalModel("BIOMD0000000001_modified.xml");
        String expectedOrganism = "";
        String actualOrganism = paramsModel.getOrganism();
        assertEquals(expectedOrganism, actualOrganism);

    }

    @Test
    public void testExtractOrganismWithoutResolvedOrganismValue() {

        ParamsModel paramsModel = extractOrganismFromLocalModel("BIOMD0000000006_modified.xml");
        String expectedOrganism = "331544";
        String actualOrganism = paramsModel.getOrganism();
        assertEquals(expectedOrganism, actualOrganism);

    }

    @Test
    public void testSpeciesWithAndWithoutAnnotation() {
        Model jsbmlModel = testUtils.getModelFromLocal("BIOMD0000000006.xml");
        Map<String, SpeciesInformation> speciesAnnotations =
            extractionService.getAnnotationsFromSpecies(jsbmlModel, extractionService.getExtractionContext());
        int actualNumSpeciesAnnotated = 0, actualNumSpeciesWithoutAnnotation = 0, expectedNumSpeciesWithoutAnnotation = 1, expectedNumSpeciesWithAnnotation = 3;
        for (String key : speciesAnnotations.keySet()) {
            if (speciesAnnotations.get(key).getSpeciesAnnotationString().isEmpty()) {
                actualNumSpeciesWithoutAnnotation++;
            } else {
                actualNumSpeciesAnnotated++;
            }
        }
        assertEquals(expectedNumSpeciesWithoutAnnotation, actualNumSpeciesWithoutAnnotation);
        assertEquals(expectedNumSpeciesWithAnnotation, actualNumSpeciesAnnotated);
    }


    @Test
    public void testSpeciesExtraction() {

        String actualInitialData, expectedInitialData;
        Model jsbmlModel = testUtils.getModelFromLocal("BIOMD0000000006.xml");
        extractionService.initializeExtractionContext(jsbmlModel);
        List<Species> species = jsbmlModel.getListOfSpecies();
        ParamsModel paramsModel = new ParamsModel();
        Optional<Species> optionalSpecies = species
            .stream()
            .filter(s ->
                s.getId().equals("v"))
            .findAny();
        assertTrue(optionalSpecies.isPresent());
        Species tempSpecies = optionalSpecies.get();
        paramsModel = extractionService.extractSpeciesInfo(tempSpecies, paramsModel);
        actualInitialData = paramsModel.getInitialData();
        expectedInitialData = "0.0 mol";

        assertEquals(expectedInitialData, actualInitialData);

        String actualAccessionUrl = paramsModel.getAccessionUrlString();
        String expectedAccessionUrl = "http://identifiers.org/uniprot/P04551|Cyclin-dependent kinase 1; " +
            "http://identifiers.org/go/GO:0031387|MPF complex; " +
            "http://identifiers.org/go/GO:0004693|cyclin-dependent protein serine/threonine kinase activity";

        assertEquals(expectedAccessionUrl, actualAccessionUrl);

        String actualSpeciesSboTermUrlString = paramsModel.getSpeciesSboTermUrlString();
        String expectedSpeciesSboTermUrlString = "https://identifiers.org/sbo/SBO:0000297|protein complex";

        assertEquals(expectedSpeciesSboTermUrlString, actualSpeciesSboTermUrlString);

        String actualSpeciesId = paramsModel.getSpeciesId();
        String expectedSpeciesId = "v";
        assertEquals(expectedSpeciesId, actualSpeciesId);

        String actualSpeciesMetaID = paramsModel.getSpeciesMetaID(),
            expectedSpeciesMetaID = "_175119";
        assertEquals(expectedSpeciesMetaID, actualSpeciesMetaID);
    }

    @Test
    public void testSpeciesExtractionNegativelyAccessionUrl() {

        Model jsbmlModel = testUtils.getModelFromLocal("BIOMD0000000687.xml");
        extractionService.initializeExtractionContext(jsbmlModel);
        List<Species> species = jsbmlModel.getListOfSpecies();

        ParamsModel paramsModel = new ParamsModel();
        Optional<Species> optionalSpecies = species
            .stream()
            .filter(s ->
                s.getId().equals("m_8_0"))
            .findAny();
        assertTrue(optionalSpecies.isPresent());
        Species tempSpecies = optionalSpecies.get();
        paramsModel = extractionService.extractSpeciesInfo(tempSpecies, paramsModel);
        String expectedAccessionUrl = "http://identifiers.org/cl/CL:0000898|naive T cell";
        String actualAccessionUrl = paramsModel.getAccessionUrlString();
        assertEquals(expectedAccessionUrl, actualAccessionUrl);

    }


    @Test
    public void testResolveSboTermPositive() {
        assertEquals("Henri-Michaelis-Menten rate law", extractionService.resolve("SBO:0000029"));
    }

    @Test
    public void testResolveSboTermNegative() {
        assertEquals("SBO:0000999", extractionService.resolve("SBO:0000999"));
    }

}

package net.biomodels.parameters;

import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Test;
import org.sbml.jsbml.Model;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import static org.junit.Assert.*;

public class ExtractionContextTest {
    private ExtractionContext ctx;

    @Before
    public void setup() {
        ctx = new ExtractionContext();
    }

    @Test
    public void extractReactionInfoMap() throws IOException, XMLStreamException {
        Model m = TestUtils.parseTestModel("BIOMD0000000001.xml");
        ctx.extractReactionInfoMap(m);
        assertEquals(17, ctx.getReactionInfoMap().size());
        ReactionInfo react0 = ctx.getReactionInfo("React0");
        assertNotNull(react0);
    }

    @Test(expected = NullPointerException.class)
    public void extractReactionInfoMapRejectssNullModels() {
        ctx.extractReactionInfoMap(null);
    }

    @Test
    public void getReactomeSet() {
        assertThat(ctx.getReactomeSet(), CoreMatchers.is(new HashSet<>()));
    }

    @Test
    public void getSabioRKSet() {
        assertThat(ctx.getSabioRKSet(), CoreMatchers.is(new HashSet<>()));
    }

    @Test
    public void getReactionInfoMap() {
        assertNull(ctx.getReactionInfoMap());
    }

    @Test
    public void loadReactomeMappingFileFromExtractionContext() {
        ctx.loadReactomeMappingFileFromExtractionContext();
        assertFalse(ctx.getReactomeSet().isEmpty());
    }

    @Test
    public void loadSabioRKMappingFileFromExtractionContext() {
        ctx.loadSabioRKMappingFileFromExtractionContext();
        assertFalse(ctx.getSabioRKSet().isEmpty());
    }

    @Test
    public void getGlobalParameters() {
        assertThat(ctx.getGlobalParameters(), CoreMatchers.is(new HashMap<String, String>()));
    }

    @Test
    public void setGlobalParameters() {
        ctx.setGlobalParameters(Collections.emptyMap());
        assertTrue(ctx.getGlobalParameters().isEmpty());
    }

    @Test
    public void setGlobalSpeciesAnnotations() {
        ctx.setGlobalSpeciesAnnotations(Collections.emptyMap());
        assertTrue(ctx.getGlobalSpeciesAnnotations().isEmpty());
    }

    @Test
    public void getGlobalSpeciesAnnotations() {
        assertThat(ctx.getGlobalSpeciesAnnotations(), CoreMatchers.is(new HashMap<>()));
    }
}

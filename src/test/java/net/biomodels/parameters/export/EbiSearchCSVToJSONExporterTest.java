package net.biomodels.parameters.export;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.biomodels.parameters.TestUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

/**
 * @author carankalle on 03/10/2018.
 */
public class EbiSearchCSVToJSONExporterTest {

    @SuppressWarnings("SameParameterValue")
    private EbiSearchJsonView convertToJsonForLocal(String filename) {

        String actualFileName = TestUtils.RESOURCE_PATH + "/" + filename;
        String args[] = {actualFileName};
        EbiSearchCSVToJSONExporter.main(args);

        File file = new File(TestUtils.RESOURCE_PATH, filename + ".json");
        file.deleteOnExit();
        StringBuilder fileData = null;
        try {
            fileData = new StringBuilder(
                Files.readAllLines(file.toPath(),
                    Charset.forName("UTF-8"))
                    .stream()
                    .collect(Collectors.joining("\n"))
            );
        } catch (IOException e) {
            fail(e.getMessage());
        }

        Gson gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .disableHtmlEscaping()
            .setPrettyPrinting().create();
        return gson.fromJson(fileData.toString(), EbiSearchJsonView.class);
    }

    @Test
    public void testJsonExportStats() {

        EbiSearchJsonView ebiSearchJsonView = convertToJsonForLocal("BIOMD0000000006_export.csv");
        int entriesSize = ebiSearchJsonView.getEntries().size();
        long entryCount = ebiSearchJsonView.getEntryCount();

        assertTrue(entriesSize != 0);
        assertEquals(7, ebiSearchJsonView.getEntryCount());
        assertEquals(entryCount, entriesSize);
    }

    @Test
    public void testJsonExportOriginalAndRawFieldsPositive() {
        EbiSearchJsonView ebiSearchJsonView = convertToJsonForLocal("BIOMD0000000006_export.csv");

        //Testing entity_RAW field
        String expectedEntityRAWValue = "[cyclin-dependent protein serine/threonine kinase activity; MPF complex]";
        EbiSearchEntry cyclinEntry = ebiSearchJsonView.getEntry(0);
        Map<String, String> firstField = cyclinEntry.getField(0);
        String actualEntityRAWValue = firstField.get("value");
        String expectedEntityRAWName = "entity_RAW";
        String actualEntityRAWName = firstField.get("name");

        assertEquals(expectedEntityRAWName, actualEntityRAWName);
        assertEquals(expectedEntityRAWValue, actualEntityRAWValue);

        //Testing entity field
        String expectedEntityValue = "\\[cyclin\\-dependent protein serine\\/threonine kinase activity; MPF complex\\]";
        String actualEntityValue = ebiSearchJsonView.getEntry(0).getField(1).get("value");
        String expectedEntityName = "entity";
        String actualEntityName = ebiSearchJsonView.getEntry(0).getField(1).get("name");

        String expectedCrossReference = "BIOMD0000000006";
        String actualCrossReference = ebiSearchJsonView.getEntry(0).getCrossReferences(0).get("dbkey");
        assertEquals(expectedCrossReference, actualCrossReference);
        assertEquals(expectedEntityName, actualEntityName);
        assertEquals(expectedEntityValue, actualEntityValue);
    }

    @Test
    public void testJsonExportRawFieldsNegative() {
        EbiSearchJsonView ebiSearchJsonView = convertToJsonForLocal("BIOMD0000000006_export.csv");
        for (Map<String, String> field : ebiSearchJsonView.getEntry(0).getFields()) {
            assertFalse(field.containsKey("reaction_RAW")
                || field.containsKey("initial_amount_RAW")
                || field.containsKey("publication_RAW")
                || field.containsKey("entity_sbo_term_RAW")
                || field.containsKey("reaction_sbo_term_RAW")
                || field.containsKey("entity_id_RAW")
                || field.containsKey("initial_concentration_RAW")
                || field.containsKey("model_RAW"));
        }
    }
}

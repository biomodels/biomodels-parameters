package net.biomodels.parameters;

import org.junit.Before;
import org.junit.Test;
import org.sbml.jsbml.ASTNode;
import org.sbml.jsbml.Model;
import org.sbml.jsbml.Reaction;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 * @author carankalle on 31/08/2018.
 */
public class DefaultGlobalParametersMatchTest {

    private DefaultGlobalParametersMatch defaultGlobalParametersMatch = new DefaultGlobalParametersMatch();
    private ExtractionService extractionService;

    @Before
    public void setUp() {
        extractionService = new ExtractionService(new RequestContext(null));
    }

    private Model getModel(@SuppressWarnings("SameParameterValue") String filename) throws IOException, XMLStreamException {
        File file = new File(TestUtils.RESOURCE_PATH, filename);
        return extractionService.getModel(file);
    }

    @Test
    public void testGetMatchGlobalParameters() {
        Model model = null;
        try {
            model = this.getModel("BIOMD0000000006.xml");
        } catch (IOException | XMLStreamException e) {
            fail(e.getMessage());
        }

        this.extractionService.initializeExtractionContext(model);
        Reaction reactionId2 = (model.getReaction("Reaction2"));
        ASTNode astNode = reactionId2.getKineticLaw().getMath();
        if (astNode == null) {
            fail("Null equation");
        }
        String actual = defaultGlobalParametersMatch.getMatchedGlobalParameters(astNode,
            extractionService.getExtractionContext().getGlobalParameters(),
            null);
        String expected = "k6 = 1.0";
        assertEquals(expected, actual);

        Reaction reactionId3 = (model.getReaction("Reaction3"));
        ASTNode astNode2 = reactionId3.getKineticLaw().getMath();
        if (astNode2 == null) {
            fail("Null equation");
        }

        String actual2 = defaultGlobalParametersMatch.getMatchedGlobalParameters(astNode2,
            extractionService.getExtractionContext().getGlobalParameters(),
            null);
        String expected2 = "k4 = 180.0; k4prime = 0.018";
        assertEquals(expected2, actual2);


    }


}

